---
title: Leo AR Partners with 8th Wall to Expand Its AR Marketplace to 90% of the World’s
  Smartphones
layout: article
categories:
- Tech
image: http://localhost:4000/magic-leap-5_ysotkk_nz5tio_lbebms-1030x476.jpg
author: Dana Loberg
date: '2018-06-13 02:00:00'
source:
  name: Venture Beat
  url: https://venturebeat.com/2018/06/13/leo-ar-partners-with-8th-wall-to-expand-its-ar-marketplace-to-90-of-the-worlds-smartphones/
tags:
- Articles
- My Startups
video:
  title: ''
  url: ''
  iframe: ''
---

**Developer platform will bring Leo AR to Android and iPhone devices without native AR support**

SAN JOSE, Calif.–(BUSINESS WIRE)–June 13, 2018–

[Leo AR](http://cts.businesswire.com/ct/CT?id=smartlink&url=http%3A%2F%2Fwww.leoapp.com&esheet=51821541&newsitemid=20180613005604&lan=en-US&anchor=Leo+AR&index=1&md5=b63bfdbed5d6c4bc7646d818b7ce1f80), the “Amazon of Augmented Reality,” and the AR development platform [8th Wall](http://cts.businesswire.com/ct/CT?id=smartlink&url=https%3A%2F%2F8thwall.com%2F&esheet=51821541&newsitemid=20180613005604&lan=en-US&anchor=8th+Wall&index=2&md5=a26bf4611171c5abeb60da4a507099f4) today announced a new partnership that will dramatically increase the number of mobile devices that Leo’s iOS and Android apps are compatible with. Dubbed the ‘Amazon of Augmented Reality,’ Leo lets people buy and sell AR objects and create unique AR experiences.

Already, Leo’s users have downloaded more than 1 million artist-created AR objects, making it the world’s No. 1 consumer-facing AR app. By working with 8th Wall and its universal AR development tools, Leo’s reach will extend to devices that do not currently feature native AR support, including the iPhone 5s, the iPhone 6 and a host of Android devices. All told, Leo AR will soon be available on 90% of smartphones across the globe.

“As a company that believes in creative expression, we see it as our mission to bring our augmented communication tools to as many people as possible,” said **Dana Loberg**, co-founder and CEO of LEO, the umbrella company for Leo AR. “We’re excited to be working with 8th Wall to expand access to our app, especially to smartphone users outside the United States.”

Leo AR enables people to transform the world around them by livening up real-world spaces with artist-created augmented reality objects, allowing users to interact with the new AR spaces they’ve created. Pretty soon, users will have the ability to purchase more unique and limited objects in a storefront to make their scenes and videos even more unique and compelling.

The company’s partnership with 8th Wall comes on the heels of several new, more socially-designed features for Leo AR — including the ability to more easily discover new videos, as well as tapping the videos to heart or like the AR creations. In addition, Leo users are now able to follow each other’s profiles, as well as copy and paste objects or ‘mirror’ objects from another user’s video into a video of their own.

“8th Wall was built to help creators expand the wonders of augmented reality beyond the next-generation devices that currently support AR,” said 8th Wall founder **Erik Murphy-Chutorian**. “In partnering with Leo AR, we’re opening the door for millions of new people to create and consume artist-created AR experiences they can’t find anywhere else.”

**About LEO**

LEO is the first marketplace for augmented visual communication, a growing medium that is revolutionizing the way people interact with each other and the world around them. LEO operates Leo AR (an AR marketplace for realistic 3D and 4D animated objects and photogrammetry) and Leo Stickers (the world’s leading emoji marketplace) — unlocking new forms of visual expression to anyone with a smartphone. For more information, visit [www.leoapp.com.](http://cts.businesswire.com/ct/CT?id=smartlink&url=http%3A%2F%2Fwww.leoapp.com&esheet=51821541&newsitemid=20180613005604&lan=en-US&anchor=www.leoapp.com&index=3&md5=6c82d34a2888d03b223733d962107cfc)

**About 8th Wall, Inc.**

Founded in 2016, 8th Wall, Inc. helps application developers redefine markets by using augmented reality to break down the walls between the physical and digital worlds. 8th Wall’s cross-platform solution lets developers build AR-enriched applications in minutes and deploy them on close to 3 billion mobile devices. 8th Wall is headquartered in Palo Alto, Calif. For more information, please visit: [www.8thwall.com](http://cts.businesswire.com/ct/CT?id=smartlink&url=https%3A%2F%2Fwww.globenewswire.com%2FTracker%3Fdata%3DWaPLhWAzF0JmWtiNrr48u355aTUSyxmNiublvTnzOG_nGSfLAzJxaPjoUkuZt579bWHIn7vSrfhKY5c31XGpRw%3D%3D&esheet=51821541&newsitemid=20180613005604&lan=en-US&anchor=www.8thwall.com&index=4&md5=1b32a5f3bc926d8aea7164eed51b3726). Follow 8th Wall on Twitter [@the8thWall](http://cts.businesswire.com/ct/CT?id=smartlink&url=https%3A%2F%2Fwww.globenewswire.com%2FTracker%3Fdata%3D3yspunMiTtBDTarXdJqCaVAlycSJI9Snd7KgqD-ioPNp5ywWVr4INCd-zgc7O2u1GTHoHJVdyUzd96g_2RxJ6Q%3D%3D&esheet=51821541&newsitemid=20180613005604&lan=en-US&anchor=%40the8thWall&index=5&md5=cef432b323db174ac1a99e01626c6756).



View source version on businesswire.com: [https://www.businesswire.com/news/home/20180613005604/en/](https://www.businesswire.com/news/home/20180613005604/en/)

FLIGHT PR
Alysha Light
[alysha@flightpr.com](mailto:alysha@flightpr.com)