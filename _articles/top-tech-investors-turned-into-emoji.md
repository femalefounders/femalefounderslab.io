---
layout: article
categories:
- 
image: http://localhost:4000/vc-emoji-techcrunch-dark_vsh3pk-1_cy2sj3-705x347.jpg
author: Dana Loberg
date: '2016-07-28 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/gallery/vcmoji/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: Top tech investors turned into emoji
---

What do Marc Andreessen, Ron Conway and Naval Ravikant look like as emojis? For the launch of emoji marketplace [MojiLaLa](https://mojilala.com/), the startup made a set inspired by tech’s best-known venture capitalists. The list could have included more of the great female and minority VCs like Mary Meeker, Charles Hudson and Aileen Lee. But here’s a look at caricatures of the 20 they chose. If you’re trying to raise funding, you better know them all.