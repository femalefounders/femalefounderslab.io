---
layout: article
categories:
- Tech
image: http://localhost:4000/variety-movielala-1-4-million-seed_lfgois_hcp1mx-1030x580.jpg
author: Dana Loberg
date: '2015-09-26 02:00:00'
source:
  name: Forbes
  url: https://www.forbes.com/sites/steveolenski/2015/09/26/marketing-a-movie-aint-what-it-used-to-be/2/#c2915a14c535
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: Marketing A Movie Ain’t What It Used To Be
---

Yes I know, I used the word ain’t. Let the grammar police and english teachers unite and come at me en masse. Forgive me but in case you don’t me by now, I tend to write the way I speak. Highly conversational.

Ok, now that that’s out of the way.

Did you know that last year in the U.S. alone there were nearly 1.3 billion movie tickets sold with a total box office gross of $10.4 billion? The stat, which comes from [the-numbers.com](http://www.the-numbers.com/market/2014/summary) speaks to the enormous popularity movies continue to be for those looking to spend their entertainment dollar.

Now these numbers are just reflective of those who actually went to a theater to view a movie. They don’t take into account DVD sales, downloads, NetFlix, and on and on. You can just imagine what the real total numbers would look like when it comes to the amount of money that’s up for grabs for those entrusted with the marketing of a movie.

**Back in the Day**

“[In the past, movie marketing was primarily a buy to get an advertisement on page 6 of the newspaper](https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.forbes.com%2Fsites%2Fsteveolenski%2F2015%2F09%2F26%2Fmarketing-a-movie-aint-what-it-used-to-be%2F&text=In%20the%20past%2C%20movie%20marketing%20was%20primarily%20a%20buy%20to%20get%20an%20advertisement%20on%20page%206%20of%20the%20newspaper) , and running commercials on Thursday night to bring awareness to people about the opening weekend for a film,” says Dana Loberg, co-founder of Silicon Valley-based social network and movie marketing startup, [MovieLaLa](https://movielala.com/). “Everything was based on traditional marketing methods (posters, billboards, commercials, etc).”

As it has with essentially everything else, social media had and continues to have a significant impact.

> “It was social media that initiated a sea change in movie marketing,” says Arthur Chan, EVP Digital Marketing, [Palisades MediaGroup](http://www.palisadesmedia.com/), a Santa Monica, CA-based independent media agency.

He says social is at the core of almost every campaign as it not only elevates word of mouth through earned media, but it’s the active link that keeps campaigns integrated through each discipline.

Francois Martin, EVP Marketing & TV Sales for [The Weinstein Company](http://weinsteinco.com/) says this shift was caused by a number of things including audience fragmentation, which increased with the rise of cable programming. DVRs which meant less people were watching commercials, in this case movie trailers. And finally, something that is happening as we speak − the proliferation of streaming and on demand services.

Chan is quick to point out that that little thing called mobile is most certainly at play in all this.

“Over the last few years, digital consumption has rapidly shifted into the mobile and “cord cutter” space,” says Chan. “Driven by the increase in smartphone/smart device adoption and the decrease in the cost of data, [all movie studios now take a platform agnostic approach to marketing](https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.forbes.com%2Fsites%2Fsteveolenski%2F2015%2F09%2F26%2Fmarketing-a-movie-aint-what-it-used-to-be%2F&text=all%20movie%20studios%20now%20take%20a%20platform%20agnostic%20approach%20to%20marketing) .”

**Roadblocks Dead Ahead**

There are no shortage of roadblocks or obstacles for movie makers wanting to market their films to the masses.

Loberg says size absolutely matters telling me “the length of the content usually needs to be short in mobile, so the traditional length of trailers and marketing have needed to change, too.” She also believes the social networks and mobile chat apps have completely taken over as the most time spent vs. television devices of the past.

She identified the following obstacles facing movie marketers today:

1.  Movie fan demographic (14-24y/o) is more mobile and tech savvy

2. Movie fan demographic (14-24y/o) is also more social (Facebook, Twitter, Snapchat etc)

3. Shorter attention spans

4. Savvier in terms of marketing (blind to banner ads and popups)

5. Fragmented audience makes marketing much more challenging (moving target across devices, apps, etc) Hopping around more frequently and less committed and loyal to one place.

**The Future of Movie Marketing**

Well one might say the future is in the “cards” as in Cardlytics, the company that pioneered the global Card-Linked Marketing industry. Earlier this year they partnered with both The Weinstein Company and Palisades MediaGroup in what was described as innovative marketing campaign “that will take a tremendous amount of guesswork out of getting proven moviegoers into theaters opening weekend.”

The movie’s opening weekend in question was the boxing drama Southpaw starring Jake Gyllenhaal which opened on July 24.

The Weinstein Company and Palisades MediaGroup used purchase data to target active moviegoers who have not only purchased tickets online, but at the box office of all theater locations. As a result, they had the ability to reach the 85%+ of active moviegoers who purchase their tickets on site.

Having the ability to target true frequent moviegoers with advertising, based on the purchasing data at the theaters is a “real game-changer” according to Martin.

> “Studios promoting movies today are armed with more tools than ever before, especially when it comes to data,” says Arlo Laitin, Senior Vice President Media.”Leveraging real purchase data to identify fans and followers is where the future of movie marketing is headed.”

As for specific thoughts on the future of movie marketing.

> “As long as movie marketers embrace the new technologies, and they adapt to the way people are consuming content as well as the way people are chatting around content — then the future looks bright for movie marketers.” – Dana Loberg, co-founder, MovieLaLa
> 
> “The pillars aren’t going away just yet, trailers and posters in the movie theater or spending on television.  You’ll see more media dollars moved to digital advertising.  I think the amount of data that movie marketers have at their disposal is only going to get better, more detailed.  We’ll be able pinpoint ticket buyers in a more granular way, on a title by title basis.  Finally, someone is going to be able to catalog all the social data, reference that with online polling and tell me how much the picture is really going to gross this weekend.” – Francois Martin, EVP Marketing & TV Sales for The Weinstein Company
> 
> “As our audiences continue to shift media consumption behaviors, so does the way we architect campaigns. With this, we’re seeing a heavy shift into content marketing now, where quality is driving successful campaigns. Also, platform agnostic media is becoming more the norm as data marketing continues to grow.  We are only a couple of years away from the ‘Minority Report’ scenario that marketers talk about.” – Arthur Chan, EVP Digital Marketing, Palisades MediaGroup