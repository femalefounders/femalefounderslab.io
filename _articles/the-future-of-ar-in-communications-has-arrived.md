---
layout: article
categories:
- Tech
image: http://localhost:4000/59a469bf1e00001600c5faa1_qsxrw9_p1ofwr.jpg
author: Dana Loberg
date: '2017-08-28 02:00:00'
source:
  name: Huffington Post
  url: https://www.huffingtonpost.com/entry/the-future-of-ar-in-communications-has-arrived_us_59a44e05e4b0a62d0987b12e
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: The Future of AR in Communications Has Arrived
---

Augmented Reality (AR) is set to hit the market in a major way soon, with the upcoming launch of Apple’s new iPhone 8 and iOS 11. Everyone is anticipating what new and cool features Apple will release next. Will it be a facial recognition software that unlocks your phone? A second camera that allows for more AR/3D objects to be placed in the camera? Or how about an edge-to-edge display screen?

With all the buzz surrounding this, I caught up with Dana Loberg, a former artist turned serial entrepreneur and co-founder of emoji and sticker marketplace, **[MojiLaLa](http://www.mojilala.com/)**. The Silicon Valley-based startup recently raised a $1.5 million seed round, solidifying its run to become the primary sticker platform. It also recently formed a partnership with GIPHY, the largest GIF search engine.

Read on as we chat with Dana about all things mixed reality, the impact of AR on how we communicate, and why we may soon use glasses to more easily integrate AR into our world.

**Steve Olenski:** So, you’re a Millennial tech CEO and this must be a very exciting time for you to witness. How do you see people using AR in this new “mixed reality” world?

**Dana Loberg:** There are so many applications of AR that will be part of our normal lives in the future. It will have huge impacts on how content is displayed; everything from education to enterprises, architecture, interior design, healthcare and everything in between. It will alter the way we communicate, the way we interact with friends, family — even objects. There are applications of AR that we haven’t even imagined yet that will make this a huge platform and even technology that could eclipse the invention of the mobile phone.

**Olenski:** There have been AR and VR companies for at least 10 years working on various innovations. Why is now the right time to be launching a company in this space?

**Loberg:** It’s true. But the technological components and processes were not there as pure tech. Either it was too expensive or the computation of processing it was clunky and difficult. Now that we have our phones (which are pretty powerful machines) and the costs of the technology have fallen, now couldn’t be a better time to launch an AR platform. Apple has smartly mastered timing and I believe now they will lead the race when it comes to AR. As a company that straddles the line between a hardware and software company, Apple glasses are surely in the roadmap over the next 3-5 years.

**Olenski:** Why do you think there is this push toward “augmenting” our physical surroundings or even altering the way we communicate through gifs, stickers, emojis — and whatever else comes next?

**Loberg:** For one, AR has the power to create a much more beautiful space around you. It also adds an element of playfulness and fun. As humans, we love to use our imagination and want to create beauty and be surrounded by it. AR is one way to make it easy and accessible for people to make their surroundings and environment more to their liking.

Secondly, in the past, only a select few, such as engineers and design experts, had the tools to augment pictures and video. With AR, anyone can alter their surroundings. It’s totally democratizing the ability to create beauty to surroundings that might otherwise be dismal or less aesthetically pleasing.

**Olenski:** What are some examples of the way you see us adapting to AR?

**Loberg:** We’ve already been exposed to AR through Pokemon Go, and Dancing Hot Dog from Snapchat. In the beginning, AR will have this quirky, funny, even novel playfulness to it that will allow anyone to play with it. Going forward, the videos will become more professional, and we might start to see AR celebrities become as popular as today’s YouTube stars. And then there will be glasses we will wear when we want to experience AR without the phone as the portal. At the point where we’re all wearing glasses to access this mixed reality, the ecosystem of AR will be much more developed with real applications and tools that are more established and mature.

**Olenski:** You started out as an artist before becoming an entrepreneur. How did that experience spark the idea for your startup?

**Loberg:** Starting a new painting or building a new startup has all the same anxieties and excitement an artist or an entrepreneur feels about their new venture. In both fields, you learn as you go, and execution is the most difficult, yet most gratifying part. There’s an eagerness to complete the painting the same way there is in completing an app (i.e. sleepness nights, odd hours and an insatiable desire to finish a project, ASAP). Just as an artist can see all the mistakes, an entrepreneur can see the weaknesses in what they’ve built. Neither ever has complete satisfaction in what they’ve created.

**Olenski:** What is going to be AR’s impact on communications in the next 3-5 years?

**Loberg:** We’re beyond texting at this point. Like, texting is soooo 2012. I look at AR as a really fun way to express yourself and get really creative with technology. Everyday people are getting access to these tools, so it’s totally leveling the playing field. I think we will all look back at the 2D world as boring and static when compared to how fun, realistic, artistic and creative AR will make our communications.

**Olenski:** You mentioned Apple glasses. You predict we’ll be wearing glasses in the future?

**Loberg:** Yes, Snap has its spectacles, Google has its glasses and even Apple is (probably) experimenting with glasses. I think Google glasses were ahead of their time and never fully realized. Spectacles was obviously a huge success for Snap and a great test run on how to make glasses cool and wearable from consumers who don’t want to look geeky. Apple will also do a great job when it comes to design and style because it’s what they do best. It’s inevitable it will move in this direction next because phones are so limiting when it comes to being immersed in time and space. It’s ‘ok’ for AR now but long term, glasses will be the product to really bring AR to the forefront.

**Olenski:** Finally, what possible role do you see brands have in AR?

**Loberg:** Smart brands will have a tremendous opportunity to leverage AR to bring their customer closer. Right now, we’re seeing IKEA building showcase rooms in AR so people don’t have to imagine but can see the interior space with Ikea furniture. There will be lots of great applications where you can see what you want to buy in AR before making the final purchase. Imagine wearing a dress in AR and seeing it on or styling accessories around your clothing to find the perfect looks for your outfit.

The most exciting things about AR and brands is that now, those who couldn’t afford Ray Ban sunglasses or their favorite Maserati can now put Ray Bans on their desk table at work and a beautiful 2017 Maserati car in their garage via AR. Again, technology continues to level the playing field and give anyone access to the objects and things they’ve always desired. The world is your oyster in the new AR world.

**Image Source:** Pexels