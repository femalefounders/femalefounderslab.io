---
title: 7 Simple Ways for High-Performers to Find Balance (+ Avoid Burnout)
layout: article
categories:
- Women in Entrepreneur
image: http://localhost:4000/pexels-photo-746386_ckuhvb_h9l87c_twqpyy-1030x608.jpg
author: Dana Loberg
date: '2018-04-02 02:00:00'
source:
  name: Thrive Global
  url: https://www.thriveglobal.com/stories/26518-7-simple-ways-for-high-performers-to-find-balance-avoid-burnout
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
---

***Here’s your checklist to stay sane, de-stress and boost productivity***

I remember a time when, sobbing on the floor of my bathroom, I puked from pure stress. The reasons lay in getting my startup off the ground. After working many months through weekends and holidays — my body simply broke down.

After several minutes of deep breathing, I regained some semblance of physical and mental control. It was my first experience with a level of stress I had never experienced before.

Every channel setting in my life was turned to “go, go, go,” opening the floodgates for all this noise and stress to fill most of my days and nights, while leaving me oblivious to how unhealthy I’d become.

Work days spilled into evenings and weekends. I canceled every event that was not work related for months, missing many celebrations of family and friends. It’s amazing how we can forget to make time for physical needs like eating healthy foods and sleeping a full eight hours when we are preoccupied with work goals. My health took a back seat to the “occupant” in the driver’s seat — a fledgling startup. With a team of 8 and salaries and families relying on me to keep food on the table, there were several stressors indirectly affecting and occupying my mind.

A former Division 1 volleyball player, I put a premium on healthy habits. Looking back, I should have paid more attention to the small signs of my deteriorating condition–from headaches and tired mornings that escalated to frequent nose bleeds and chest pains.

Six years have passed since those days of starting my first company, and the memories of being in that state are reminders to help me keep my health in check today. Without our health we have nothing, and we certainly can’t keep our businesses going.

I have since developed a lot of rituals around maintaining healthy habits. Integrating these practices into my daily life is easier than carving out time for them, which could tempt me to skip them.

Here are the top seven rituals to which I adhere daily or weekly to maintain mental and physical health:

1.  **Walking**. Or some other low-impact exercise during the day. Either I walk to work, take short walks between meetings, go for a walk after work. There are many ways to get some cardio in the day, even if it means hopping out of a cab further away from your destination taking the stairs instead of the elevator.
2.  **Acupuncture**. I’m fortunate to live in California where acupuncture is covered by my health insurance. My acupuncturist and I have a standing weekly appointment. This ancient Chinese medicine has been practiced for more than 3,000 years, and for me, it provides a balance between yin and yang energies. Most energies that you can’t see can have major impacts on the nervous system.
3.  **Massage**. Sometimes I add massage work to the acupuncture appointment to work out some of my body aches. Sometimes it can feel a little overindulgent when it comes to massage, but now I don’t think twice about it – it’s always worth it.
4.  **Nature therapy**. On weekends I can be found in the redwood forests around Silicon Valley to commune with nature and re-energize. There are lots of studies proving how much nature can improve your brain like this and this.
5.  **Breathwork**. There are classes offering breathing techniques, and I take them to help me release any negative thoughts. Breathwork or holotropic breathing is very therapeutic.
6.  **Yoga**. Being in the clean environment of a yoga class with a live instructor is refreshing to release the stresses of work. If I can’t make it to a class, I practice some yoga poses at home to guided music and a well-practiced YouTube instructor.
7.  **Meditation**. Listening to a guided meditation before bed really helps. I compiled a list of 10, 30-minute guided meditations that put my mind at rest.

As entrepreneurs and high-performers, it can be challenging to maintain balance when you’re leading a company that feels like it is on a space shuttle going a million miles a minute, but incorporating these regular activities can help de-stress and improve your quality of life.