---
title: Cutting-Edge Visual Creations
layout: article
categories:
- Women in Entrepreneur
image: http://localhost:4000/FullCover_Work_1118.jpg
author: Dana Loberg
date: '2018-11-13 02:00:00'
source:
  name: Where Women Create
  url: https://www.wherewomencreate.com
tags:
- Women in Entrepreneur
- Tech
- ''
video:
  title: ''
  url: ''
  iframe: ''
---

**DANA LOBERG** is an artist-turned serial entrepreneur in Silicon Valley with a successful exit plan always under her belt. She’s the current co-founder and CEO of LEO, the first and largest marketplace for augmented visual communication. LEO operates Leo AR (an AR marketplace for realistic 3D and 4D animated objects and photogrammetry) and Leo Stickers (the #1 emoji marketplace).

> “Attract what you expect. Reflect what you desire. Become what you respect. And, mirror what you admire.”
—UNKNOWN

While I currently live in Cupertino, our business, LEO, is based in Almaden Valley, near Los Gatos, CA. I grew up in West
Los Angeles, in an area called Westwood Village, and lived in a household of three girls, with me being the youngest. I believe my parents always wanted a son because my dad is such a manly man. He enjoyed fishing and hunting and was an avid rower in the marina, often waking up at 5:00 AM every morning to row out on the water. My dad was also a sailor who loved to race on Wednesday nights. He was a ‘steaks and potato’ guy who even had a separate ‘men’s room’ showcasing his rowing and sailing trophies, amongst walls hung aplenty with prized killings from hunting trips, rounded out with the token Sports Illustrated calendar.

My father was an orthodontist and professor at UCLA, who worked ten minutes from our house, while my mom was a former art teacher turned stay-at-home mom. When i was in high school and my sisters were in college, my mom went back to work with my father for fifteen years as his ental assistant. My dad was the patriarch of the family, making education the most important factor in our lives. We were also very well disciplined as children, having to learn to play the piano at seven years old and doing a lot of extracurricular activities to keep us in line and busy. All three of us had very different personalities and attributes,  yet we all excelled in school, allowing each of us to attend a different Ivy League college for our furthered education. My mom was our greatest supporter and cheerleader who drove us to all of our events and also prepared home cooked meals (we weren’t allowed to drink soda or eat fast-food). My mom was the one you went to when you were hurt or crying, and my dad was the one you went to for reviewing essays or discussions about sports. 

Growing up, I not only excelled in school (I really loved school), but I was also a naturally talented artist. I went to painting classes throughout my entire childhood. I also played the piano for over ten years and took a written, auditory and performance test called the “Certificate of Merit,” all with a competitive spirit. I was a very good athlete and played tennis, softball and soccer, finally selecting volleyball to specialize in as my chosen sport. I was recruited across the USA for volleyball and chose Yale to study and play Division I Volleyball.

My competitive spirit and drive to accomplish were supported by surrounding family members. My dad was an entrepreneur with his own private practice in orthodontics, and most of my extended family members were lawyers, judges, or engineers. My sisters and I were taught early on just how difficult it was to make money. We would host lemonade stands as children to collect money, and I even got my first job at fifteen working at Nordstrom’s. Our family quickly ingrained in us that it was important we learn from a young age to respect hard work. 

I started my first startup in technology in 2010, and I’ve been a serial entrepreneur ever since. I naturally, and perfectly, fell into the role of an entrepreneur with my disciplined upbringing and lifestyle, having learned to multi-task and time manage between my many activities involving sports, arts and education. Having very little downtime between activities and school laid a foundation of continuing to lead a super active lifestyle, capable of juggling many different things simultaneously. 

I have always been more than willing to work weekends and late nights, and I really have enjoyed the challenges that have come with being an entrepreneur. I get to learn something new every day, and this has been priceless to me. I don’t know if I went out seeking to become an entrepreneur, but I found I did not fit well into the corporate work structure, primarily because of my dislike for politics. I tend to quickly become bored doing the same thing over and over and have found that the corporate world just isn’t flexible enough to regularly expand a person’s skill set, as doing so often threatens fellow coworkers and can eliminate someone else’s job entirely.

As a business owner now, I realize how difficult it can be. If you’re in a corporate job you worry about being fired, but when it’s your own business, you worry about paying rent, keeping food on the table, avoiding lawsuits, having proper insurance policies, finding good employees, and a myriad of other critical matters to keep your business afloat.

The risk of tech start-ups can add to the worries of a techentrepreneur because of the different business model approach often applied. At our company, we utilize a “Silicon Valley-like” business model where you first build a team and the product and get it out to as many users as possible to use it. You then raise capital and proceed to work on thebusiness model that will support the company. You must constantly keep in contact and show growth in your business as it ages to ensure success.

Other successful technology products and startups in similar spaces often provide me with my next ideas and inspiration. It’s very easy to download many of their successful apps and understand how and why they got to where they are; however, chatting with other entrepreneurs in similar spaces can also spark new ideas and innovation for our current business.

When acting upon sparks of inspiration, challenges can and will often occur. For us, we were quickly successful in the digital sticker space with 2D stickers and had 2,000 artists and 35,000 stickers in less than a year; however, upon expanding from a 2D sticker marketplace into a 3D realistic object marketplace we faced our biggest challenge so far. Now, as we expand into 3D objects, the artists’ market is slightly different in this space, and we’ve had to make some modifications to our approach.

It’s not easy owning your own company, but being your own boss is incredibly rewarding. As someone who’s looking to get the most out of life, and believes in the highs and lows of living as a human, being an entrepreneur stretches the limits and really expands your life in many ways you weren’t expecting.

## 7 HARDWORKING PIECES OF ADVICE
1. Find a mentor or someone more experienced than you to help you when you need it. Even finding someone with a different business but who’s a good listener and can give good advice when you need it.
2. It helps to make a list in the morning so you stay on track and finish what you’ve set out to do for the day. I find it gratifying to check off the task when it’s completed.
3. Don’t sacrifice your health for your business. It’s better to have a happy and healthy owner than one who is rundown all the time. Hire someone if you need it. Take a vacation when you need it. Treat yourself to workouts or massage or whatever makes you happy on a regular basis.
4. Always follow your gut. If you get a weird feeling about someone during the interview or within their first week of working, part ways. Learn to fire fast.
5. Your circle of friends with whom you socialize will definitely decrease as you start on your new ventures, and this is ok. You will find new, more like-minded entrepreneur friends who will better understand you and the path you’re on.
6. Being a new business owner is pretty isolating at times because you work so hard, in the beginning, to get it off the ground. Making sure you have one close friend to chat with is so important. Also being aware that the venture might not work, and failing or closing is OK. Take it as a learning experience; even if it fails, you’ve now become experienced in opening and shutting down a business.
7. I find it satisfying to give back. So if you have the opportunity to join panels or talk to women who are looking to start their own business, take the time to educate others who are looking to shift into entrepreneurs. Giving is more satisfying than taking.