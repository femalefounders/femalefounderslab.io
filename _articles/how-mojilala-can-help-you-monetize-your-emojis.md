---
layout: article
categories:
- 
image: http://localhost:4000/2008_1200xx7360-4152-0-0_sdajjq-1030x580.jpg
author: Dana Loberg
date: '2017-02-10 02:00:00'
source:
  name: Business Journal
  url: https://www.bizjournals.com/sanjose/news/2017/02/10/how-mojilala-can-help-you-monetize-your-emojis.html
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: How MojiLaLa can help you monetize your emojis
---

The San Jose startup provides a way for artists and designers to distribute and monetize their emojis across platforms.

MojiLaLa has developed a marketplace for emojis, the images and icons used to express an idea in digital communications. It provides a way for artists and designers (dubbed “emojipreneurs” by the company) to instantly distribute and monetize their emojis across chat platforms and keyboards globally. It also has developed a native iMessage app for further distribution called MojiLaLa Unlimited. This offers unlimited access on a subscription basis to more than 12,000 emojis on a wide variety of local and global topics.

How it makes money

San Jose-based MojiLaLa shares the profits from the iMessage sticker apps with the artist, business or celebrity that created them. It has licensing agreements with chat app providers and keyboard partners. It also gets revenue from the MojiLaLa Unlimited subscriptions, which cost $1.99 a month.

Business it may disrupt

Current providers of emoji content, which it says don’t offer as many choices and don’t keep up to date on current hot topics and trends.

Management team

The company was founded by CEO [Dana Loberg](https://www.bizjournals.com/sanjose/search/results?q=Dana%20Loberg) and technology chief [Sahin Boydas](https://www.bizjournals.com/sanjose/search/results?q=Sahin%20Boydas). Loberg is a former artist who started in the creative development and production department at Fox Studios. She later was a copy writer and creative associate at ad agencies including McCann Erickson and JWT. Boydas previously founded several companies, including MovieLaLa, a personalized and socially designed platform for upcoming movies.