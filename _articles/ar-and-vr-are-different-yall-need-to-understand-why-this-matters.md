---
layout: article
categories:
- Tech
image: http://localhost:4000/magic-leap-5_ysotkk_nz5tio_lbebms-1030x476.jpg
author: Dana Loberg
date: '2018-03-14 02:00:00'
source:
  name: Venture Beat
  url: https://venturebeat.com/2018/03/14/ar-and-vr-are-different-yall-need-to-understand-why-this-matters/
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: AR and VR are different — y’all need to understand why this matters
---

One of the most anticipated launches of 2018 is the augmented reality [glasses](https://www.wareable.com/ar/magic-leap-need-to-know-release-date-price-specs-features) from Magic Leap, which are reportedly set to ship soon. Although cloaked in secrecy, the design and power of this product have been getting increasing hype. German media giant Axel Springer, which has been investing successfully in digital startups for at least the past five years, recently invested in Magic Leap, which itself has raised billions in funding over the past couple of years.

Something’s up.
## Separating AR and VR
To date, the consumer marketplace has not embraced AR glasses even though they enable the projection of virtual images into real-world environments — the next step in screenless multitasking. (Remember Google Glass?) Magic Leap, on the other hand, holds the promise of an embraceable product which may finally force the marketplace to stop lumping AR and VR together.

Why should VR be treated different;y? It’s an entirely different capability that completely takes over a person’s environment with an immersive experience and is much more suited to entertainment experiences.

VR has been around a long time, with the first head-mounted display in 1960 (the [Telesphere Mask](https://www.vrs.org.uk/virtual-reality/history.html)) which predated the mobile phone. Since then, it’s gone through various iterations of different headset creations and styles, all finding mass adoption difficult. The rise of the smartphone and massive improvements in depth sensors, cameras, motion controllers and human interfaces has seen AR grow slowly, but it exploded in 2017 with the twin releases of Apple’s ARkit and Google’s ARCore.

Due to the perfect timing of hardware and software in mobile, AR adoption has been much faster and easier for users. However, since VR technology predated AR — and because they are both related to a reality that is digital — these two technologies got placed together as a broader topic on digital layering  and have been lumped together ever since. However, as these two categories develop and evolve (each in different realms) it’s important that we start to treat them as separate technologies.

As the utility and behavior of AR and VR move forward, the tech industry must begin to reference AR and VR as distinct offerings and two wildly different animals. Many times the media mixes AR/VR in the headlines, prompting investors and many in the tech community to use these terms interchangeably. In actuality, these technologies are very different. And if innovators in the space continue to lump them together, then how will mainstream consumers know the difference?
## The key differences
Here are a few reasons why each of these technologies deserves to stand alone and apart from one another. We should take time to communicate the technologies separately as we consider the following differences:

1.In VR, users need a clunky and expensive headset to be an observer and participant in the storyline. The prices of these expensive headsets will run anyone from $600 for HTC Vive to $400 for Oculus Rift. There are cheaper versions but the quality can be compromised and experience less effective without the state of the art brands.

2.AR uses a smartphone camera as its portal, so that means just about anyone can access AR’s offering, which is more utilitarian. Users do not need to wear a headset and can remain in their actual physical sensory environment to complete tasks. Just activate the AR app, point the smartphone camera and shoot.

3.AR uses the real space around you and adds digital 3D objects to combine the real world with the AR objects/world. AR imposes a digital layer on top of the real world, enabling users to remain in environment. It’s as if the real world is the backdrop to the AR objects or space one creates. A business user can project a spreadsheet while a consumer can virtually move virtual 3D objects such as [furniture](https://www.wired.com/story/ikea-place-ar-kit-augmented-reality/) around their living space. Snapchat had a popular dancing hot dog that could be placed into any camera-view,  as another popular example of this mixed-reality world.

4.AR is primarily a tool to help complete tasks. Since it isn’t immersive, users have more control over AR. VR is much more about having exhilarating experiences, and the designers of that experience have the most control over the environment because most of the story is predetermined. The user merely “witnesses” through the headset what has been prepared. By contrast, AR users can add new objects to existing spaces and create any kind of functional computing space they want.

As entrepreneurs, innovators and investors in this category, we need to be clear on the distinctions and uses for AR and VR. It’s our responsibility to help distinguish between these two technologies now, and make an effort to them seperate them. This way, when they’re do become mainstream, consumers won’t be misinformed on what they’re witnessing and have a better understanding of them as separate technologies.

*Dana Loberg is CEO and co-founder of Leo AR, the first augmented reality communications platform that gives anyone the power to enrich the world around them with realistic 3D and 4D animated objects and photogrammetry. Follower her [@luckyloberg](https://twitter.com/luckyloberg?lang=en).*