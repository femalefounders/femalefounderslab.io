---
layout: article
categories:
- Women in Entrepreneur
image: http://localhost:4000/mojilalatechchrunch_lxaagf_ybmw1n-1030x680.jpg
author: Dana Loberg
date: '2017-08-08 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2017/08/08/mojilala-the-sticker-marketplace-lands-1-5m-seed-round-from-great-oaks-betaworks/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: MojiLaLa, the sticker marketplace, lands $1.5M seed round from Great Oaks,
  betaworks
---

The launch of iOS 10 and its iMessage Store has launched emojis and stickers into mainstream communication. In fact, [6 billion emoticons are sent every day](https://www.emarketer.com/Article/Who-Needs-Words-You-Have-Emojis/1012466). And a new startup called MojiLaLa is looking to capitalize, both figuratively and literally.

[MojiLaLa](https://mojilala.com/) is a marketplace that lets artists submit their sticker packs to the iOS store with zero hassle, for free.

The company has just raised $1.5 million in seed funding from Great Oaks Ventures and betaworks ventures, and IVP partner Dennis Phelps.

Founder and CEO Dana Loberg, a former artist, says that the process of submitting art to the App Store requires learning XCode, owning a Mac and iPhone, and $99. MojiLaLa does all the heavy lifting for artists for free, letting them focus on their artwork instead of their business development.

MojiLaLa lists these sticker packs within iOS on behalf of the artists, and splits revenue 50/50. Loberg says that, eventually, the company plans to expand artist revenues to an 80 percent share once the visual communication revolution is in full swing.

Alongside listing sticker packs for artists, MojiLaLa also has its own app (MojiLaLa Unlimited) that lets users subscribe to get complete access to over 25,000 stickers. Users pay $1.99/month to get access to these stickers, and artists who opt-in to list on MojiLaLa unlimited get an extra level of visibility for their work.

As we advance closer to mainstream AR/VR experiences, a relationship with artists become incredibly important. Artists are the ones who create the face filters we use in Snapchat, and the virtual objects we’ll interact with in Facebook Spaces, and that list will only continue to grow.

MojiLaLa wants to position itself as the over-arching representative of those artists in the tech world, handling all the back-end expertise and heavy lifting while giving artists the opportunity to put their work on the platforms we use the most.

MojiLaLa has integrations with platforms like Kika Keyboard, Baidu, Giphy, Line, Facebook Messenger, Microsoft, Skype, Gfycat, and Telegram, with more than 2,000 artists on the platform.

Eventually, the company wants to start brokering partnerships between brands and artists for branded sticker packs. For now, however, MojiLaLa is focused on scaling the platform and expanding the engineering team.