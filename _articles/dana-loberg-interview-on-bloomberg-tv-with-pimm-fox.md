---
title: Dana Loberg Interview on Bloomberg TV with Pimm Fox
layout: article
image: http://localhost:4000/bloombergtvdana-screenshot_mkwelg_bacmll-1030x568.jpg
author: Dana Loberg
date: '2013-12-06 02:00:00'
source:
  name: Youtube
  url: https://www.youtube.com/watch?v=4G6MTLxdtEE
tags:
- Articles
video:
  title: Dana Loberg Interview on Bloomberg TV with Pimm Fox
  url: https://www.youtube.com/watch?v=4G6MTLxdtEE
  iframe: <iframe width="560" height="315" src="https://www.youtube.com/embed/4G6MTLxdtEE"
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
---

