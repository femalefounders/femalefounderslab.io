---
title: How Working As An Artist Made Me A Better Entrepreneur
layout: article
categories:
- Women in Entrepreneur
image: http://localhost:4000/180426-girlboss-dana-loeberg_l6q5k5_huqlwn_ueqv30-1030x451.jpg
author: Dana Loberg
date: '2018-04-26 02:00:00'
source:
  name: Girl Boss
  url: https://www.girlboss.com/work/dana-loberg-advice/
tags:
- Articles
- Female
video:
  title: ''
  url: ''
  iframe: ''
---

*Dana Loberg has built a successful career in both as an artist in New York and, later, as a tech entrepreneur in Silicon Valley. In the latest edition of, “Nobody Tells You This, But…,” Loberg recounts how her experience trying to get her artwork showcased later helped her pitch investors in California.*

I grew up in LA and my mom was an art teacher, so art was just something I did. But I was one of those kids that had a lot of interests in a lot of places and was always trying to use the right and left side of my brain.

I moved back to San Francisco in 2008 during the economic crisis after having worked in the corporate world. I wanted a different experience and, while there were a lot of reasons to move back, I picked San Francisco because I knew that’s where the tech sector was and I knew that technology was going to have a big impact on humanity.

That’s when I was bit with the entrepreneur bug.

Silicon Valley’s pretty small, so you get to meet a lot of people and everyone’s kind of an entrepreneur. It felt like a whole new world for me and I loved it. You’re meeting tons of people and everyone is working on building something. You put in long hours and you’re attached to your phone a lot, but I enjoy the hustle.

It wasn’t until my second company with my current co-founder that I felt like I had my first real, official start-up. In a twist of fate, I’m back in the art space, building a sticker marketplace for artists across the world.

It’s funny how things work out, but in my role as an entrepreneur, I rely a lot on my experience as an artist in New York. As an artist in the city, I would go to the Chelsea art galleries and I would meet every gallery owner. I was so persistent about connecting with gallery owners that I eventually managed to have my work shown in Starbucks stores all over the city. Even then, I didn’t recognize myself as an artist-entrepreneur.

When you’re trying to be an artist, you’re usually stuck trying to balance your time. You do your work at night, while also trying to make the business connections you need to get your work shown.

It was so, so hard trying to do that in New York. Struggling through without any kind of direction or mentorship gave me valuable stamina that helped me later in Silicon Valley. When I went into tech and I started pitching investors, it was easier than what I was trying to do as a solo artist on the other coast.

You also find that you have a new network you can tap into when you’re founding a company and pitching investors alongside a co-founder. It’s a whole different level of support and mentorship, and giving presentations and communicating efficiently and effectively with investors didn’t feel so stressful. The artist I grew up as helped me become the entrepreneur I am now.

*[Dana Loberg](https://www.danaloberg.com/) is an artist turned serial entrepreneur currently working in Silicon Valley. She’s  cofounder of [Leo AR](https://www.leoapp.com/), a marketplace where users can buy virtual objects, and she’s also the cofounder of [Leo Stickers](https://mojilala.com/), a sticker marketplace for independent artists and studios from all over the world.*