---
layout: article
categories:
- Tech
image: http://localhost:4000/surrealtechrunch_plirte_s7txgy-1030x686.jpg
author: Dana Loberg
date: '2017-09-27 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2017/09/27/surreal-wants-to-be-the-one-stop-shop-for-3d-virtual-objects/
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: Surreal wants to be the one-stop shop for 3D virtual objects
---

Don’t expect the [ARKit](https://techcrunch.com/tag/arkit/) app rush to end any time soon.

[MojiLala](https://techcrunch.com/2017/08/08/mojilala-the-sticker-marketplace-lands-1-5m-seed-round-from-great-oaks-betaworks/), the betaworks-backed marketplace for emojis and emoticons, is today launching their second app.

Meet [Leo](https://www.leoapp.com/), a marketplace that lets users buy virtual objects. The app opens up to an outward-facing camera and lets users choose animated virtual objects, which they can place in a photo or video.

Right now, Surreal only offers about 50 items, but founder and CEO Dana Loberg says that there are around 100,000 objects waiting to be added to the platform.

Eventually, the app will understand the context of the situation — if you’re out by the pool, in your house, or outside in your backyard — and serve up objects that make sense in that situation. For example, Surreal has a variety of sea-based animals like jellyfish and octopi and will serve those up to you when it recognizes a body of water.

Once a user has taken a photo or video, they can save that photo and share it across other social networks. Surreal also has its own feed that lets users share to the platform for other Surreal users to see. In fact, if you see a post on Surreal that you like, you can use the Mirror feature to copy the objects used in that photo or video to use yourself.

This is an expansion of MojiLala’s existing offering, which is an expansive library of emojis. Artists submit their sticker packs and split the revenue with MojiLala, or receive more visibility by participating in the MojiLala Unlimited subscription (which gives users access to a broader library of stickers for a monthly fee).

Loberg hopes that Surreal will be able to offer the same deal for artists, selling their virtual objects and splitting revenue with the artist (80 percent would go to artists, and 20 percent would go in MojiLala’s pocket).

Despite the fact that brands and developers are diving headlong into ARKit, it’s unclear if your average consumer cares that much about it. This may very well be a race to the bottom, the same way that Google Glass apps ended up being a big waste of time and resources.

Will the wonder of creating AR photos and videos have a lasting effect with users?

MojiLala and Surreal are set to find out.