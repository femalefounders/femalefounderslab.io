---
layout: article
categories:
- Tech
image: http://localhost:4000/motorcycle-heads-up-display-venturebeat_nuzwch_imwfuh-1030x527.jpg
author: Dana Loberg
date: '2017-11-30 02:00:00'
source:
  name: Venture Beat
  url: https://venturebeat.com/2017/11/30/5-industries-primed-to-leap-forward-into-ar/
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: 5 industries primed to leap forward into AR
---

With a powerful new camera and advanced sensor technology, the latest round of iPhone products [has unlocked new possibilities](https://www.theverge.com/2017/9/12/16272904/apple-arkit-demo-iphone-augmented-reality-iphone-8) in the realm of augmented reality (AR). Now more than ever, today’s developers are fully equipped to craft next-level experiences that blur the lines between the consumer’s screen and the physical world around them.

For brands, these developments present a massive opportunity to engage customers in a way that sets themselves apart from the competition. At a time when businesses are struggling to gain organic attention on mobile devices, marketers in a wide array of verticals can enhance their customer’s real-life surroundings by using sponsored 3D item or a unique multimedia overlay. With the right AR experience, the end user will be able to see something they’ve never seen before, simply by dragging a 3D item into their camera view or waving their phone’s viewfinder across a physical space.

Here are some ideas on how five industries can adopt AR right now.
## Live events
In September, Apple [demonstrated how Major League Baseball is using AR](https://youtu.be/VMBvJ4MTXzc?t=1m15s) to enhance the live experience inside the stadium. When users hold the viewfinder over the field of play, those fans can now see real-time information and stats for the game they’re watching. While this is certainly an exciting use, it’s only scratching the surface of what’s possible for AR in live events.

<iframe width="560" height="315" src="https://www.youtube.com/embed/VMBvJ4MTXzc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Live events are a natural use case for AR, as they present an environment where people will already be using their cameras to snap photos and videos. At a concert, AR has the potential to amplify a musician’s stage presence with 3D props or an animated digital overlay. When done right, AR can teleport audience members into a new, immersive experience that is complementary to the performance they’re attending.

In addition to providing value to the consumer, live-event AR experiences can give the sports league or concert promoter new opportunities to sell brand sponsorships.
## Fashion/beauty
As anyone who’s ever ventured through a department store knows, trying on a bunch of clothes can be an exhausting, time-consuming process. But rather than spending an entire day going in and out of fitting rooms, an effective AR app would allow consumers to “try on” a new outfit — without having to pull clothes off the rack. This creates a shopping experience that is more fun and more efficient for the customer. In a similar vein, [Topology Eyewear](https://www.topologyeyewear.com/) is a new AR app that uses face-scanning technology to help you design customized glasses, requiring that customers simply take a video selfie of their face with their iPhone.

AR can be especially useful when it comes to other accessories such as earrings, which people don’t always have the chance to try on in-store — and cosmetics. Imagine AR letting a shopper know what a shade of lipstick, makeup or eyeshadow will look like when applied to her face, instead of forcing her to guess based on what the products look like when smeared on her wrists. AR also allows people to try more cosmetics products in one sitting, as they’ll no longer need to wash everything off when they want to test a new shade.
## Transportation
AR allows public and private transportation companies to make commuting a little more predictable and a whole lot easier.

By creating AR-powered bus stops, these organizations could allow riders to see a live map showing exactly where their bus is and how long it will take to reach them — just by holding their phone up near the stop. This will give people the information they need to quickly decide whether they should continue waiting or start looking for an alternate route to get to their destination. Augmented reality [can also be tremendously helpful for vehicle drivers](https://venturebeat.com/2017/08/23/ar-will-drive-the-evolution-of-automated-cars/).

The key lies in using data to create a more transparent, more convenient experience for the customer. When done correctly, a transportation AR experience also creates a valuable sponsorship placement for consumer brands.
## Travel/hospitality
When walking through an unfamiliar city, tourists are unlikely to know much about their surroundings. Maybe the dive bar they’re about to pass is hosting an up-and-coming indie band later in the evening, or maybe the hotel one block over has an incredible restaurant inside. Either way, this lack of knowledge creates a missed opportunity, both for the traveler and the business they never get to patronize.

But with AR, cities and businesses can build apps that make it easier for people to explore. For instance, placing the camera viewfinder over a Las Vegas hotel could show people the restaurants, shops and shows inside it. With an especially sophisticated app, people could even buy tickets to a concert or book time at a spa directly from the AR experience. For tourist destinations of all kinds, AR is a great way to turn ambient foot traffic into incremental sales.

## Quick-service restaurants
Given fast food’s popularity among children, quick-service restaurants can use AR to turn the in-restaurant experience into an interactive wonder world. By moving their phone’s viewfinder across various parts of the restaurant, kids could see all kinds of fun, playful activities.

For instance, a child might find Wendy’s mascot crouching behind a table or running around the play area. By tapping on these mascots, kids could initiate educational games or interactive videos. Really, there’s no limit to the number of fun, creative ways QSR brands can use AR to engage with children while they’re eating or waiting to order.

AR presents a number of avenues for companies to engage their target customers. For now, it’s up to marketers to be proactive in their pursuit of AR innovation. The only way to develop an experience that truly resonates is to be willing to try new things and refine best practices as you go.

*[Dana Loberg](https://twitter.com/luckyloberg) is co-founder of [Surreal](http://www.surrealmarket.com/), an augmented reality communications platform that gives people the power to augment the world around them using realistic 3D and 4D animated objects — bringing the AR world to anyone with a smartphone.*

**Image Credit:** DigiLens
–DigiLens enables augmented reality overlays.–