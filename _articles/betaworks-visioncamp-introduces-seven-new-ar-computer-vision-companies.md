---
layout: article
categories:
- Tech
image: http://localhost:4000/leo2-e1522857525811_oqhiwk_zja5lt_sah98i.jpg
author: Dana Loberg
date: '2018-04-03 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2018/04/03/betaworks-visioncamp-introduces-seven-new-ar-computer-vision-companies/
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: Betaworks VisionCamp introduces seven new AR/computer vision companies
---

More than ten years ago, [betaworks](https://www.crunchbase.com/organization/betaworks/)  launched to foster an ecosystem of startups focused on the intersection of media and consumer behavior. While the mission hasn’t changed, the structure has seen some tweaks. The company has introduced its own venture arm, led by Matt Hartman, as well as the more recent launch of betaworks Studios.

But nestled gently between the two are betaworks Camps program. Camps are a sort of hyper-specific accelerator program, within which a small cohort of early-stage startups build out their products within a certain theme, complete with the full resources of betaworks (marketing, legal, space, etc.) as well as a small investment.

Camps first launched with BotCamp, followed shortly by VoiceCamp, and today the graduates of [VisionCamp](https://www.crunchbase.com/organization/visioncamp/)  are showing off their wares for the first time at Demo Day.
## Leo
Originally called [Surreal](https://techcrunch.com/2017/09/27/surreal-wants-to-be-the-one-stop-shop-for-3d-virtual-objects/), [Leo](https://www.leoapp.com/) offers a vast marketplace of AR objects, stamps and artwork so that users can change the world around them. [Leo](https://www.crunchbase.com/organization/leo-2/)  has raised $1.5 million in seed and has relationships with upwards of 2,000 artists on the platform. The company, which was founded by Dana Loberg and Sahin Boydas, makes money by sharing revenue with artists who create objects for the platform.
## Camera IQ
[Camera IQ](http://cameraiq.co/) calls itself a camera experience manager. The company works with brands and publishers to develop virtual worlds for customers, with partners including Spotify, Neiman Marcus and Viacom. The technology integrates AR toolkits and mobile OSes with brands native apps to offer different experiences for consumers. [Camera IQ](https://www.crunchbase.com/organization/camera-iq/)  was founded by Allison Wood and Sonia Tsao. The founders say that the camera represents the next great consumption experience, as well as the next great transaction experience. The company hopes to sit at that intersection.
## Facemoji
Livestreaming and FaceTime are now accessible to everyone, but not everyone wants to show their face on these platforms. Enter [Facemoji](https://facemoji.co/). The startup offers 3D avatar webcams that streams your facial expressions via the avatar without ever showing your real likeness. The company was originally focused on gamers who stream on Twitch, with plans to expand to video chat. Facemoji was founded by Robin Raszka and Tom Krcha.
## Numina
Nearly half of land area in cities is made up of streets, sidewalks and parks, and cities have no data or insights on these spaces. [Numina](http://www.numina.co/) partners with cities to place computer vision sensors on light poles in these areas and offer anonymous flow data about pedestrians in these spaces. The company offers an API for streets, as well, to give developers access to real-time activity and a backlog of activity for their apps, whether it’s for mobility, insurance, real estate, or logistics. Numina was founded by Tara Pham.
## Selerio
[Selerio](http://www.selerio.io/) brings together the real world and the virtual world by using computer vision to map the layout and objects in a room and replace them with a virtual world. Imagine putting old-school Victorian furniture inside an existing space. The company uses deep learning and computer vision in its technology, which was spun out of Cambridge University. Selerio offers an SDK to developers and is currently being integrated with Apple’s ARKit. Selerio was founded by Ghislain Tasse.
## Streem
[Streem](https://www.streem.pro/) supports the professional home services industry by using computer vision, machine learning, and AR to capture vital information (like model, make and serial number) through a simply live video chat. Through [Streem’s](https://www.crunchbase.com/organization/streem/)  technology, service pros can capture information, take measurements and save notes without ever stepping foot in the client’s home, letting them offer quotes much faster and solve the problem in one try. Streem was founded by Patrick Ezell and Jef Holove.
## Trash TV
Despite the fact that capturing and editing video is more accessible than ever, video editing remains a time-consuming and tedious process. The [Trash TV](http://www.gettrashed.tv/) app uses computer vision and AI to edit consumer videos into something beautiful and usable. The company uses a stock video repository that includes proof of creation to fill in the gaps. Trash TV was founded by Hannah Donovan and Anton Marini.

This is the third of betaworks’ Camps. The next one, according to Camps General Manager Danika Laszuk, is focused on the intersection of live streaming and participatory audiences. Dubbed LiveCamp, betaworks hopes to find startups evolving the space as Twitch streaming and apps like HQ continue to pull in large viewerships and the lines between performer and audience are blurred.