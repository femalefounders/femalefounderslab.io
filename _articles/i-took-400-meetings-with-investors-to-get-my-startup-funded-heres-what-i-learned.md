---
title: I Took 400 Meetings With Investors to Get My Startup Funded. Here’s What I
  Learned.
layout: article
categories:
- Entrepreneur
image: http://localhost:4000/post.jpg
author: Dana Loberg
date: '2018-06-14 05:58:50'
source:
  name: Entrepreneur
  url: https://www.entrepreneur.com/article/314310
tags:
- Articles
- Female
- My Startups
video:
  title: ''
  url: ''
  iframe: ''
---

***Here are five strategies that early stage entrepreneurs can adopt as they begin fundraising for their company.***

Yep, you read that headline right — 400 meetings with investors. As a Silicon Valley entrepreneur who has raised seed and angel money in practically every major U.S. tech hub, I’d like to share a few tips about what I’ve learned along the way.

**Related: [How to Use Psychology to Get Investors to Close When You Want Them to](https://www.entrepreneur.com/article/313181)**

The first thing you need to know is there are major differences between raising money in New York, San Francisco and Los Angeles. The unique culture of each city plays a distinct role in how venture capitalists approach working with startup founders, so you need to be ready for this — and you may eventually find that one market totally gets your company’s niche while others don’t.

In L.A. and New York, for example, VCs tend to focus more on the revenue model of your business and are pretty blunt in asking about it. “How much money are you making?” is a question that comes up early on. San Francisco investors, on the other hand, are very focused on data. They want to know more about traffic and retention: Their thinking is that if a startup has the traffic, they can monetize it several ways with a high eyeball count.

Along my journey to raising capital, I’ve learned a few tips and tricks that have made me a better, more successful fundraiser. Here are five strategies that early stage entrepreneurs can adopt as they begin fundraising for their company:

## 1. Shuffle the deck.
If an investor asks for your presentation deck before your meeting, find a clever way not to send it. Let that person know you are still updating it or completely “forgot” to attach it. It is super easy for investors to see your deck (which is only a partial story about your company) and say “no.” Getting the first meeting in person is crucial so that they remember you, which increases your chances of being able to close the deal.

For example, oftentimes I write something like, “would love to grab a quick coffee and chat more about our business” as a way to secure a meeting. Since it seems more casual and entails less of a commitment, it usually works — and I’ve found that many times during these meetups, I don’t even show the pitch deck, which is fine. It’s much better to have a one-on-one with an investor over a casual conversation so you can make an impression; you can always share your deck later as a follow-up.

**Related: [I’ve Raised Over $20 Million for My Businesses. Here’s How to Get the Attention of Venture Capitalists.](https://www.entrepreneur.com/video/313398)**

## 2. Court the CEO.
For any introductory meeting, make sure it’s just you and the CEO/founder. Any follow-up meeting can be with the CTO or No. 2 executive, but it’s far more effective to tell your story clearly and concisely to the most important decision-maker, one-on-one.

Personally, I find that having two-against-one in a conversation can be overwhelming and at times feel strained. There were times when I brought my co-founder to initial meetings with investors, and they would oddly look at him a lot more, even though I did most of the talking. Maybe they were more comfortable asking questions of a man than a woman. Whatever the reason, this made those intro meetings more awkward for me and at times frustrating. Keeping the energy positive is crucial to ensure both you and the investor to walk away pleased with the meeting.

## 3. Value your time.
I’ve heard countless stories from new founders about investors who don’t show up or cancel meetings last minute. Don’t sweat it — take it as a clear sign of what kind of investor they might be (someone who clearly doesn’t value your time). Watch out for early signs of time-wasters, and if this happens to you, move on.

Once, literally as I was parking my car for a meeting, I got an email from the assistant that the investor was, not able to make the meeting. Although being late or bailing with moment’s notice can be a frustrating part of L.A. culture, I don’t usually bother to reschedule these meetings unless the reason was valid.

**Related: [7 Reasons to Never Send Your Deck to an Investor Before You Meet in Person](https://www.entrepreneur.com/article/312393)**

## 4. Avoid the divas.
If you hear through the grapevine about investors who are prickly or difficult, run as fast as you can in the opposite direction. Or at the very least, take that into serious consideration if they decide to invest in your company. There were times when I ignored talk about particular investors — dismissing it as gossip — only to later discover that there was truth behind it. Do your due diligence, and find out as much as you can about an investor’s reputation.

## 5. Stifle the follow-up questions.
Investors having initial follow-up questions after your meeting is to be expected. But, if there are repeated questions, watch out. A few questions are OK but after the first set, they pretty much know if they want to invest in your company or not. I’ve come to see repeated follow-up questions post-meeting as not a sign of interest, but of an investor fishing for reasons to say no. No one who’s ever had a long list of follow-up questions has invested in my company, and going through the back and forth has only proved to be a waste of time. Don’t fall for the trap, and instead focus on investors who are genuinely interested in you and your business.