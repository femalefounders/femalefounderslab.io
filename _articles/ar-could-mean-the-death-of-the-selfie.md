---
title: AR Could Mean The Death of The Selfie
layout: article
categories:
- Tech
image: http://localhost:4000/apple-camera-fashion-5164.jpg
author: Dana Loberg
date: '2018-10-31 02:00:00'
source:
  name: Venture Beat
  url: https://venturebeat.com/2018/10/31/ar-could-mean-the-death-of-the-selfie/
tags:
- AR, VR, Tech
video:
  title: ''
  url: ''
  iframe: ''
---

We may be in the middle of a great shift from a “me” to a “we” culture. Many cultural commentators have noticed that much of humanity has hunched over technological devices, isolated and oblivious to both the outside world and the activities going on around them. While this has helped people develop laser-sharp focus on the tasks at hand, it’s also a form of sensory blockage bordering on apathy.

The emergence of augmented reality changes this. AR technology changes the terms of our relationship to [mobile devices](https://www.framesdirect.com/blog/how-augmented-reality-has-changed-selfies-forever). We will no longer be tethered to texting on the tiny screens that help us on our daily lives. Instead, the screen’s camera becomes our main platform and portal, making us face outward because AR puts our screens into our actual surroundings.

## The death of the selfie
We live in a selfie world. Instagram, an app based around performative selfies and presenting a carefully curated version of one’s own life, is one of the most popular apps on the internet. Selfies are the fuel behind Snapchat and much of the app economy. A first wave of AR-powered apps already lets users give themselves animal or cartoon character faces or see how they would look wearing different makeup or different clothes. But this is only the beginning.

The next wave of AR will replace selfies with manipulating the environment around us. Snapchat- or Instagram-style filters will be passe when users have dinosaurs on their beds or aliens in their living room. Video conferencing will go to the next level when someone 2000 miles away will look like they’re sitting in the office with you. People won’t even be able to tell what’s real or fake in the videos or photos you take because the AR Van Gogh looks like the very original painting in the Louvre in Paris, except now it’s on your living room wall.

By definition, selfies are acts of egotism and all about the self. AR, however, is about communal experience and joint manipulation of the spaces around us. As AR technology matures and cameras, processors, and sensors become more powerful, we will transition from the individualistic world of the selfie to the communal world of AR.

## Understanding our environments
In addition, AR is a great way to visualize and see the world in 3D right before your eyes. For instance, AR show doctors or pre-med students how the heart works. Or even how the blood flows within the vessels.  There have also been efforts to use AR overlays for things like disaster relief. Because AR is about “we” instead of “me,” there are many more communal use cases.

Aftershocks and damaged [buildings](https://www.forbes.com/sites/forbescommunicationscouncil/2018/05/14/how-augmented-reality-will-innovate-bim-visualization/#733489b66fb5) put rescuers in danger when saving earthquake victims. Using AR, someone already in a disaster area can share their surrounding environment with someone in a safe zone. These partners can help rescuers assess what needs immediate attention and the best way to access that area. This allows for more efficiency for disaster relief in the aftermath of an earthquake or flood.

## Changing Physical Space
AR, whether in our phones now or on [glasses-like headsets](https://venturebeat.com/2018/08/15/microsofts-hololens-mall-demos-bring-early-ar-glasses-to-the-masses/) in the future, will help humanity focus attention once again on the larger world around us. We will no longer have to look down or tap a small icon on a small screen for utility purposes. Instead, we will use the world around us to make purchases, write emails, connect with friends, and all the other daily habits we currently do on a screen.

In addition, we could see a new era of performing [computer-related tasks](https://www.wired.com/story/augmented-reality-desktop-cmu/) in our physical space. Computing can be projected onto physical tables or the side of a building. This results in more physical movement, more openness to our environments and the freedom to be more creative in our daily lives. If nothing else, we could come away with better posture and a sense of exploration.

AR focuses our attention on whole landscapes and spaces, rather than the self (or selfie). These new landscapes are not about ourselves and our faces, but about our friends, neighbors and communities. In time, I hope AR will help us digitally create an outside world that better expresses who we are, who we want to be and the type of environments we want to spend most of our time in.

*Dana Loberg is CEO and co-founder of [Leo AR](https://www.leoapp.com/), the first augmented reality communications platform that gives anyone the power to enrich the world around them with realistic 3D and 4D animated objects and photogrammetry. Follower her [@luckyloberg](https://twitter.com/luckyloberg?lang=en).*