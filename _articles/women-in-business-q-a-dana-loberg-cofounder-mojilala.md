---
layout: article
categories:
- Women in Entrepreneur
author: Dana Loberg
date: '2017-06-10 02:00:00'
source:
  name: Huffington Post
  url: https://www.huffingtonpost.com/entry/women-in-business-qa-dana-loberg-cofounder-mojilala_us_593bdcf7e4b014ae8c69e0b9
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: 'Women in Business Q&A: Dana Loberg, cofounder, MojiLaLa'
---

Dana Loberg is cofounder of MojiLaLa, a sticker marketplace for independent artists and studios from all over the world.

**How has your life experience made you the leader you are today?**

There are several experiences that prepared me for the startup world, when I look back at the path that led me to where I am today: one is a previous job experience, and the other was an influential environment which impacted my character to prepare me for the startup world.

Throughout my life, I’ve been a painter. Art has always been a central part of who I am. I used to paint large pieces of art between jobs, after work and weekends, even staying up late at nights to finish the art pieces. I led a very simple life, saving money for art materials and finding creative ways to stay afloat. I think that really prepared me for the early stages of the startup in the bootstrapping stage. I truly believe if I’d hadn’t had the experience of being a struggling artist, I would have found the early stages of startup life unbearable. Bootstrapping a startup means not receiving income and finding lots of unique ways to make ends meet so that majority of the money is being spent on engineers who are actually building your product.

A second life experience which led me to succeed in a startup was the actual environment of living in New York City after I graduated from Yale. Living in NYC kind of forces you to up your social skills, which becomes a really important skill in a startup. Nobody wants to invest in a socially awkward entrepreneur. Plus, in a startup, you’re constantly meeting new entrepreneurs, investors, partners, potential team members, and all sorts of people that can have major impacts on your business. Being able to communicate clearly and effectively are important traits for startup leaders which I believe, were traits I perfected while living in NYC.

**How has your previous employment experience aided your tenure at MojiLaLa?**

Out of college, I worked at some of the largest advertising agencies in the world McCann Erickson NY for several years and J. Walter Thompson. Having worked in the corporate world for for a while, I have a deep understanding of how the corporate ladder works, which has been really important for MojiLaLa for doing partnerships and business development. Knowing the management levels, who you need to convince, who actually signs off on the documents, etc. — is important for learning how to create the most successful partnerships. I’ve also worked at a startup in San Francisco where I was employee No. 2, so I know what it’s like to be an early team member at a startup. I think that’s important so you can better understand what your team is going through—and the ups and downs of #startuplife.

Also, the obvious answer is being an artist myself; I know the struggles of the occupation. I don’t want artists to struggle anymore, and I want to find better ways to merge technology and art together. In addition, I want to monetize artists’ work in this mobile focused world. Building an emoji marketplace for artists everywhere, we are not only allowing them to get broader distribution and exposure of their art via sticker packs, but we’re also creating jobs for artists who match with brands looking for designs of their own. Since I have the mindset of the artist, I know that focusing on the creation of art is all artists want to do. Building out the technology or figuring out how to get distribution is the last thing an artist wants to spend time on. It’s also not usually in the artist’s DNA to focus on partnerships, distribution and ‘selling’ tactics. Frankly, we tend to hate the ‘sell out’ marketing ways of making it ‘mainstream’ and conforming to commercial art. So I really want to support artists in continuing to make stickers that represent them, their passions, their culture, and the art they’re happy with at the end of the day.

**What have the highlights and challenges been during your tenure at MojiLaLa?**

Some of the highlights always include receiving very kind emails of gratitude from artists all over the world (even writing the emails in their own language). The artists are so excited to be working with an art and technology company like ours, MojiLaLa, that’s supportive and helpful for getting sticker artists the best distribution of their sticker art. Waking up every day to kind messages and appreciation has been really something very special and unique for us as a startup. Seeing all the happy artists who are excited for our platform, plus seeing the extreme growth of the art community we’re building who join and the submissions of their cool sticker art(completely organic) has been gratifying as a founder. I don’t have to spend any money to get sticker artists as they swarm and join on a daily basis looking for better distribution of their stickers.

Our biggest challenge is being able to identify the best stickers packs that people love, because stickers are so subjective to the end user, that it’s a certain skill set trying to figure out which ones will be a hit or miss.

**What advice can you offer to women who want a career in your industry?**

My advice to women who want a career in technology or want to build their own startup, is to start now. Have meetings with other entrepreneurs, learn how they got started, and begin to network while you’re thinking of which idea to pursue. It’s never too late to start your own business.

**What is the most important lesson you’ve learned in your career to date?**

Always have a mentor who is not in your category, someone who you can trust, who hopefully has more experience, and therefore, more insight on how to help you overcome your problems. It’s really refreshing to meet with your mentor time to time to have an honest and open conversation about the current state in your startup. It’s important to find someone who is positive, open, non-judging and a good listener.

**How do you maintain a work/life balance?**

This is an ongoing challenge for most entrepreneurs, and all other hard working people, in finding the right work/life balance. A lot of the lines tend to blend between personal and work when it’s your own company. I try to be consistent with doing some sort of exercise throughout the work week, even if it’s taking a phone call outside while I’m walking. I also make sure to do yoga once a week. Meditate before sleep. Light candles to relax in the evening. Small things that can make a huge difference on relaxing the brain. On the weekends I make sure to take a few hours completely disconnected from me phone. This could be something as simple as taking a hike in the mountains where there literally is no reception or taking a long bath and relaxing afterwards (phone in airplane mode).

Whether you’re working at your job or enjoying some needed rest and relaxation, being fully present and mindful while you’re in that moment is important to get the most out of that time spent.

**What do you think is the biggest issue for women in the workplace?**

The biggest issues for women in the workplace, especially in technology, is that it tends to be more male dominated. I think, in terms of numbers in tech companies, women are always a minority. That can make it more of a challenge in the workforce when you’re constantly reminded you’re one of a few women in a very male-dominated office space.

As soon as you put aside the gender inequality, women can realize that this can be a huge advantage as well. So what starts as an ‘issue’ can really change into something more positive, if you let it.

**How has mentorship made a difference in your professional and personal life?**

Mentoring has helped me tremendously in my professional and personal life. My mentors tend to be other experienced entrepreneurs who I can relate with on many levels. Without their guidance and advice, I would have a much more difficult time building our startup.

**Which other female leaders do you admire and why?**

There’s a lot of amazing women out there. One woman in particular is Apple’s SVP of Retail, Angela Ahrendts. She’s a great woman that I admire.

I got to meet with Angela at a private event, in San Francisco. She’s ranked 25th in Forbes’ most powerful people in the world. Yet, she was really humble, genuinely fascinated by new technology, kind, grateful and interesting. There are a lot of words to describe her but I really admire her for taking the leap as the CEO of Burberry to lead in Apple retail. I can imagine the amount of pressure and expectations, and having met her in person, I really admire her. I think at her level and that time period in her life, it would have been pretty easy to coast along and finish her days at Burberry, but she went for the more challenging route of joining a very tech-heavy company like Apple and that takes a lot of balls. And for that I admire her the most on a long list of inspirational women.

**What do you want MojiLaLa to accomplish in the next year?**

Our company is really growing at a very fast pace at the moment. In the next year, I want to maintain that exponential growth and also, make revenue quickly for our company so we’re less reliant on funding. There’s a lot of ways to monetize in the sticker marketplace beyond directly selling paid sticker apps to users, so I’m really excited to explore the options and see what really works when it comes to generating revenue for artists and the company.