---
title: MovieLaLa CEO Dana Loberg says tech will help you find a better film
layout: article
categories:
- 
author: Dana Loberg
date: '2014-06-12 02:00:00'
source:
  name: Business Journal
  url: https://www.bizjournals.com/triangle/bizwomen/news/profiles-strategies/2014/06/movielala-ceo-dana-loberg-says-tech-will-help-you.html?page=all
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
---

[Dana Loberg](https://www.bizjournals.com/bizjournals/search/results?q=Dana%20Loberg) grew up in the movie business. She was born and raised in Westwood and worked as an intern in the creative department on the Fox lot. College took her to Yale, and she spent six years writing advertising in New York City before relocating to San Francisco, where she got involved in PR for Silicon Valley startups.

With [MovieLaLa](https://movielala.com/), she’s come full circle, combining her background in Hollywood with the tech know-how she gained up north to launch a social network and marketing platform for upcoming movies.

The iPad app helps users build a personalized Hollywood Feed that alerts them to coming attractions, share with friends and find buddies to go to the movies with. The platform also has the potential to help studios leverage user data and analytics to better target audiences and increase buzz for their theatrical releases. Based in San Francisco, Loberg spends half her time in L.A. building out relationships with nearly a dozen Hollywood film companies. iPhone and Android apps are in the works.

The eight-person firm, including Loberg’s co-founder Sahin Boydas from Istabul, has raised $750,000 to date from a mix of Hollywood and Silicon Valley investors including Marc Benioff of Salesforce.com; Allen DeBevoise of Machinima; Jim Moloshok, a former executive at HBO and Warner Bros.; Larry Braitman, a founding investor in Flixster; Silicon Valley movie producer Angie Wang; Adam Nash of Wealthfront; and Manatt Digital Media Ventures.

Loberg chatted with me recently about MovieLaLa, how Hollywood can use it to market its films and changes in store for future versions of the mobile app.

**What is MovieLaLa?**

We believe when it comes to movie discovery and finding upcoming movies, there’s no one that’s really built the next generation for movie discovery. We look at IMDb, Flixster, Rotten Tomatoes — it’s kind of back in the 1990s. They haven’t utilized the Facebook open graph. How do you make a movie platform more social by finding friends or being able to text your friends through the app? So we wanted to build the next generation of movie discovery for moviegoers. And we wanted to focus in the iPad and mobile space because that’s where obviously people are consuming content the most.

**How might a consumer use MovieLaLa?**

We want to be the first ones to tell you of an upcoming film. No one ever notifies you of movies that you’re interested in based on the star or celebrity that you’re following. If you end up clicking and watching the trailer, [we] will let you know when it’s in theaters — building up that relationship from the first time you see a teaser to the time that it’s in a theater, eight months later. There’s a huge gap from the marketing aspect [from] the first time you see a teaser trailer and all the content that comes in between to build up anticipation. We want to have you share the content as much as possible because we’re targeting the hardcore movie fans that love trailers and love movies. Then closer to the time it’s in theaters, you have the ability to [go to] a local theater near you because we’ll tell you the time. If it’s Friday, we’ll be like, “Hey, we know you love ‘Maleficent.’ You watched the trailer. The movie’s now in theaters. Buy a ticket.” So the experience is more seamless instead of having to [dig] around the Internet when you want to go to a movie.

**What are the social aspects of MovieLaLa?**

Honestly, we’re still sorting this one out. For right now, we let you know of friends that you’re related to or related to [a] movie [you’re interested in] that you can easily share the movie [with] or invite those people, and then you can text back and forth through the app. And then there’s a RouLaLa, we call it, where you can find someone new. We’re still actually pinpointing that where it can be more based on location. It’s kind of like a Tinder where you’re like 75 percent [matched] but more movie-based, so people can find new friends. But we’re working with someone in the mobile app department who’s a specialist in entertainment, and we might end up changing that, honestly, where people can join a group, because it’s harder to invite people one-on-one than it is to have a destination—like 7 p.m. Friday night, Westwood, [pick a] theater, and then people can join those groups. That kind of builds a community around movie lovers and movie fans.

**How can MovieLaLa work with studios to market their films?**

We will be working with the studios because ultimately building out better data, real-time analytics on user behavior is very important to them. Did people watch the full trailer? Did they watch half of it? Did they watch 20 seconds? Did they share it? Where did they share it? Who are they sharing it with? Who’s the most influential person? And build out that relationship with people because if you send them notifications, then they feel special that they’re the first ones to receive the trailer or the poster. And there’s this great content, honestly, that never gets surfaced. Studios spend millions of dollars making 10 different poster for “The Great Gatsby” or “Hunger Games” or whatever, and they just get hidden behind 10 layers of pages on certain websites.

So we just want to reward the people that are avid fans, and for the studios, collecting the data is really big for them because they’re still into tracking numbers three weeks prior to theatrical release. If you can get them better data as early on as possible, then they can know better who to target, who not to target, and maybe adjust along the way as far as marketing budget.

**What has the response been to the app in the few days since you launched?**

It’s very, very new, and we’re quickly trying to do our iteration. For the next honestly three to six months we’ll be changing things pretty drastically. Our next version’s a little bit different—better. We still have to add an onboarding process. We’re going to fix the navigation.

I think people really love the design. People are like, “Oh my god, this is so clean and really well done!” The response has been positive. There’s not a lot of people that have really been mobile and iPad-focused, which is what we are, and our designs are trying to surface the content better. They get this beautiful, sophisticated experience, so you don’t have to waste time. We’ll let you know of upcoming movies, and then based on if you click, watch or follow, then when it comes in theaters, you’ll have a list of all the movies you’ve liked in the past eight months. So it will be really cool. We kind of keep track of things for you. And the response has been really positive, but we’re still working really hard on our end too vis-à-vis user behavior and the next iterations too to improve it.

**Where does the name MovieLaLa come from?**

My co-founder was the one who came up with it. He was looking on YouTube under Lady GaGa, and there was a song [with the lyrics] “lala” in it, and it was perfect. It’s L.A., it’s LaLa, it’s La La Land. … And it’s actually geared toward a little bit more feminine and women because there are more female moviegoers than men, and they are the more social ones, so they drive boys to the movies. So it was like the perfect name. And everyone really remembers it, whether they like it or hate it. I’ve gotten polar-opposite opinions of it. I think it’s perfect. It has “movie” in it. It has the “LaLa.” It’s fun. It’s for teenagers and young people.