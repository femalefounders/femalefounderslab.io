---
layout: article
categories:
- 
image: http://localhost:4000/hollywood-0-1_duc4pr-1030x579.jpg
author: Dana Loberg
date: '2015-04-09 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2015/04/09/why-hollywood-needs-to-make-technology-a-priority/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: Why Hollywood Needs To Make Technology A Priority
---

Every day, it seems there are new headlines on the strained relationship between technology and entertainment. Variety’s recent “[Broken Hollywood](http://variety.com/2015/film/news/broken-hollywood-the-bizs-top-players-call-out-ways-industry-needs-to-change-1201416866/)” story was essentially a battle cry from entertainment heavyweights sounding off on the biggest challenges they’re facing in an industry undergoing a great deal of turmoil. In 2015, when we’ll see an estimated 1 billion smartphones sold — double the amount of personal computers — it’s no wonder traditional companies entrenched in a system dating back to newspaper and television are having a hard time adjusting.

I’ve shuttled between the Bay Area and Los Angeles 46 times over the last two years and have met with many Hollywood executives. Running a tech startup focused on the entertainment and pre-theatrical marketing of movies, I’m witnessing all the changes occurring in the film industry at a macro-level. And even more so, the shift in how movies are being produced and marketed. It’s fascinating to see the speed at which things are changing in the technology world, and the difficulty studios are having at keeping pace.
## Promising signs
Studio execs are keenly aware of the digital and mobile revolution. They know all about social layers in mobile apps, and that big data can be used to understand more about consumer behavior than ever before.

> Hollywood’s DNA is all about spending large sums of money to produce and market movies, taking risks on new technologies is not part of its make up.

Disney’s [purchase](https://techcrunch.com/2014/03/24/disney-maker-studios/) of YouTube multichannel network Maker Studios and its thousands of entertainment channels signaled a potential paradigm shift in the way studios distribute franchises and look for the stars of tomorrow. The studio even announced in December that it will enlist Maker to create short videos with popular teenage YouTubers in order to promote “Star Wars: The Force Awakens” before it opens in December. What better way to reach young people who weren’t even born when Luke Skywalker, Han Solo and Darth Vader first blasted their way into America’s hearts?

Others are also discovering new ways to inform target audiences about when their favorite movies are coming to theaters. As a result, a greater chunk of theatrical marketing budgets are now being transferred to the digital realm. Just look at Universal’s promotion of Ouija on Snapchat, or Lions Gate’s work with Instagram celebrities to get the word out about Divergent.

At the other end of the consumption cycle, many studios have sought new ways to combat the problem of Internet piracy by working with a company called Zefr to identify bootlegged clips of their movies and TV shows on YouTube and place ads on top of them.
## Challenges remain
While studio visionaries are aware of the pace of change, there is still resistance from executives who are comfortable with the old system and cling tightly to their antiquated roles. Remember, some of these studios have been around for more than a century. While new regimes appear every few years and rebuild things from the ground up, one key aspect has remained the same: bureaucracy. And as huge international conglomerates have bought out the studios, even more layers of red tape have been piled on.

Hollywood’s DNA is all about spending large sums of money to produce and market movies, taking risks on new technologies is not part of its make up. The idea of testing out new products and services, whether free or paid, proves challenging as those in power hold on to “what works” and what they know. So, in many instances, that means repeating the same methodology that’s been around for decades, or relying on a previous film’s success and repeating those methods already set in place.

When you compare that business model to the lean and nimble startups in the tech world that can update, revise and pivot their core product in a few days or weeks depending on how consumers respond, the differences could not be more striking.
## Bringing tech in-house
Even here in Silicon Valley it can be difficult to keep up with the pace of technology. As a startup, we have to constantly stay abreast of all things new. That often means taking meetings at Facebook, Pinterest, Twitter, Google, etc. to stay informed about changes occurring to the API, algorithm, and other significant updates related to how entertainment is displayed and consumed on that particular platform.

Despite the many advancements at their disposal, film studios are still fundamentally traditional in their approaches. The way to remedy this is to stop relying on third-party companies and bring the technology innovators under their own roof.

> It would be exciting to meet a studio technical team filled with engineers and data scientists tasked with replacing the technological gap in their content creation and delivery systems. In a world where technology is going to continue infiltrating our lives, film studios are at risk of becoming less important and less powerful if they don’t make technology a priority.

It would be exciting to meet a studio technical team filled with engineers and data scientists tasked with replacing the technological gap in their content creation and delivery systems.

Just as the introduction of sound and Technicolor changed the way films were produced and consumed almost 100 years ago, it’s time for the studios to be leaders once again and find new ways to create, market and distribute their content in a mobile-centric media landscape. Outsourcing these solutions is merely a Band-Aid on the very issue everyone is ignoring.

Technology is meant to improve our lives and bring added convenience to activities that were once cumbersome. Once film studios think longer term and adopt new systems that are both effective and cost-efficient, they will reap the benefits over the long run. If they don’t, then smaller, more tech-savvy media companies will level the playing field and take a bigger share of Hollywood’s profits sooner than its bigwigs realize.