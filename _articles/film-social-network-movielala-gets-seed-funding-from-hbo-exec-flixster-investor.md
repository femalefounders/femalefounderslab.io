---
title: Film social network MovieLaLa gets seed funding from HBO exec, Flixster investor
layout: article
categories:
- 
image: http://localhost:4000/film-social-network-movielala.jpg
author: Dana Loberg
date: '2013-11-12 02:00:00'
source:
  name: Venture Beat
  url: https://venturebeat.com/2013/11/12/film-social-network-movielala-gets-seed-funding-from-hbo-exec-flixster-investor/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
---

Aiming to be a social networking destination for movie fans and studios alike, [MovieLaLa](https://movielala.com/) announced today that it has received seed funding from several investors.

Terms of the deal weren’t disclosed (really, for a seed round?), but the names attached to the funding are interesting: They include Jim Moloshok, a former HBO and Warner Bros. executive; Flixster founding investor Larry Braitman; and Wealthfront COO Adam Nash.

MovieLaLa lets you follow movie stars and films and, naturally, recommends others for you to pay attention to as well. The site also serves as a marketing platform for Hollywood studios, allowing them to reach viewers with specific tastes. Big media companies are already paying attention to what their fans are saying on existing social media sites, but MovieLaLa could position itself as a more direct way to figure out what fans want.

“Studios today have no problem generating awareness leading up to theatrical releases, but they struggle to transform that into intent to purchase tickets,” said co-founder and CEO Dana Loberg in a statement. “We look forward to closing the missing loop in movie marketing by helping studios optimize their marketing dollars through more effective online targeting with movie fans.”