---
layout: article
categories:
- 
image: http://localhost:4000/movielalatechcrunch_ud3kvm_rik4ji-1030x687.jpg
author: Dana Loberg
date: '2018-01-25 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2018/01/25/gfycat-picks-up-movielala-a-small-movie-social-network-for-fans/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: Gfycat picks up movie-marketing app MovieLala
---

[Gfycat](https://gfycat.com/), a player on the smaller side of the consumer-facing explosion of GIFs, has generally spent a lot of time trying to build out a suite of creator tools — and now it looks like it’ll get another piece to add to that with a small acquisition today.

The startup said it’s acquiring MovieLala in a deal where the terms were not disclosed. MovieLala was originally birthed as a social network for movie lovers but through the acquisition, it seems that the goal will be to give Gfycat access to its network of marketers that will start promoting popular movie content on the service. MovieLala helps studios craft some campaigns for their movies for fans.

With experience dealing with Hollywood, getting MovieLala inside the company itself should help Gfycat gain some ground in working directly with studios whose content is already showing up in GIF form on the Internet (whether it’s trailers or other kinds of content). Fans are clearly already tapping into excitement over these movies, and GIF-making startups, in general, are finding an opportunity to work with brands (or studios) to find ways to create content that they can monetize and help grow into a revenue stream.

Gfycat recently said it [has around 130 million monthly active users](https://techcrunch.com/2017/10/24/gfycat-hits-130m-monthly-active-users-as-short-form-video-heats-up/) and has been [throwing its weight behind applying technology](https://techcrunch.com/2017/12/13/gfycat-wants-to-fix-your-low-fidelity-gifs-with-machine-learning/) to improve the fidelity and quality of the content that shows up. While going up against other GIF platforms, Gfycat puts a lot of emphasis on creative tools (such as [GIF Brewery, which it acquired in November 2016](https://techcrunch.com/2016/11/02/gfycat-snaps-up-mac-app-gif-brewery-to-expand-its-gif-creation-capabilities-to-the-desktop/)). All this has led to an effort where it looks to be a go-to spot for people making content, and not just finding it and uploading it.

MovieLaLa [closed a $1.4 million seed round in January 2016](http://variety.com/2016/digital/news/movielala-1-4-million-seed-round-hollywood-angel-investors-1201678139/) from investors including a host of Hollywood executives like former Walt Disney CFO Jay Rasulo and former LucasFilm COO David Anderman.