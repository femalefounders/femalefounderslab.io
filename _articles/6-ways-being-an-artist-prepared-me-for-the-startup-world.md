---
title: 6 Ways Being an Artist Prepared Me for the Startup World
layout: article
categories:
- Entrepreneur
- Tech
image: http://localhost:4000/being-an-artist-prepared-me-for-the-startup-world-entrepreneur_nthqvu-1_x0ujyy-1030x541.jpg
author: Dana Loberg
date: '2017-10-20 02:00:00'
source:
  name: Entrepreneur
  url: https://www.entrepreneur.com/article/302114
tags:
- Articles
- Female
- My Startups
video:
  title: Artist Who Bares Mastectomy Scars Creates Multimedia Documentary Project
    to Support Anyone Making a Big Life Decision
  url: https://www.entrepreneur.com/video/302508
  iframe: "<iframe width='640' height='480' frameborder='0' allowfullscreen src='//player.ooyala.com/static/v4/stable/4.19.3/skin-plugin/iframe.html?ec=E4dDQwZDE69A13AiK6E1uiDZ76tXvlsd&pbid=666a390a01504fe788e591c9f3b38f40&pcode=doaGYyOgPxsf9v2vPKkXMppUTpsU'></iframe>"
---

Opinions expressed by *Entrepreneur* contributors are their own.

Someone recently asked me how I got into the tech and startup world, and the question reminded me of a quote I once read from [Steve Jobs](https://www.entrepreneur.com/topic/steve-jobs).
He said, “[You can’t connect the dots looking forward; you can only connect them looking backwards](https://www.youtube.com/watch?v=UF8uR6Z6KLc). So, you have to trust that the dots will somehow connect in your future. You have to trust in something — your gut, destiny, life, karma, whatever. This approach has never let me down, and it has made all the difference in my life.”

**Related: [5 Traits That Made Bill Gates, Elon Musk and Jeff Bezos Wildly Successful](https://www.entrepreneur.com/article/302735)**

As I was tracing back all my decisions and steps to where I am today, I realized that it really all came from my roots as an [artist](https://www.facebook.com/DanaLobergArt/).

There are so many similarities between starting a new painting and starting a new company. From the outside, these two worlds look completely different but they are really rather similar.

Here are six “dots” that I’ve connected between the art world and tech:

## 1. There are no rules for artists regarding what their next creation will be.
The same goes for building a tech startup. Any medium or visual style is on the menu. [Elon Musk](https://www.entrepreneur.com/topic/elon-musk) comes to mind as an example of [working in a medium in which he is not an expert](https://www.entrepreneur.com/article/288904). He’s not a car expert or a NASA employee, yet his company invests in and develops new technologies in these fields.

## 2. Pitfalls and problems aren’t obvious until the work is well underway.
The hard work is in the execution and challenges. With a painting, the gratification comes once a section is completed to satisfaction. The same applies in getting a tech startup off the ground. Challenges come up and their degree of difficulty aren’t known until you surmount them after several long and sleepless nights.

**Related: [3 Ways to Inspire Yourself and Do Your Best Work When It Matters Most](https://www.entrepreneur.com/video/302734)**

## 3. Artists and entrepreneurs see problems that they don’t like, fixate on them and then figure out how to fix or improve upon them.
In both disciplines, learning comes through experience and applied experience from the “collision” of many variables.

## 4. Neither the artist nor the founder knows if anyone will like their finished product, nor do they know if people will embrace and use a product once it’s brought to market.
The confidence in pursuing either comes from the belief that as long as you like the particular art piece or consumer product, others out there will, too.

## 5. Artists and entrepreneurs enjoy the struggle.
For artists, the struggle to support themselves financially so that they can create art, is common. For tech startups, founders struggle to find engineers and other specialized professionals they can [afford to pay](http://blog.indeed.com/2017/01/24/overcome-the-tech-talent-shortage/). Sometimes, they must outsource such projects in order to make ends meet, or work an additional job to support the early build. In both areas, the early sacrifice and struggle are very real in terms of relationships, healthcare, delectable food and recreational activities to continue to build a long-dreamed of piece of art or company.

**Related: [This Celebrity Florist Has Worked With the Kardashians, Oprah Winfrey and More. He Shares 8 Tips for Pursuing Your Passion.](https://www.entrepreneur.com/video/302308)**

## 6. Accepting feedback or criticism from others of the finished piece is not easy.
The toil and sweat expended each day over months and at odd hours to finish a masterpiece or get a new company on track make any suggestions of “improvement” tough to swallow.

I don’t know if I’d be where I am today without having an art background. I believe it has given me a deep appreciation for my engineering team and their dedication and talent to bringing tech products to life for a mass consumer market. I know how difficult and challenging it can be, even though I’m not a technical engineer, because dedication and talent are so intrinsic to the arts as well.

The focus on [design in tech](https://qz.com/1075296/the-next-big-winners-in-tech-will-be-the-companies-that-choose-heart-over-head/) makes me happy that there is a sense of aesthetics among scientists and mathematicians. I couldn’t have found a better place to be than taking my skills as an artist and applying them to [technology](https://www.entrepreneur.com/topic/technology) while opening up doors for other artists to take the plunge. There’s nothing more gratifying.