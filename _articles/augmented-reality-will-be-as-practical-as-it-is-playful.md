---
title: Augmented reality will be as practical as it is playful
layout: article
categories:
- 
image: http://localhost:4000/swift-shot-2_u6frib-1030x682.jpg
author: Dana Loberg
date: '2018-06-21 02:00:00'
source:
  name: Venture Beat
  url: https://venturebeat.com/2018/06/21/augmented-reality-will-be-as-practical-as-it-is-playful/
tags:
- Articles
video:
  title: Surreal - Redefine What Is Possible
  url: https://www.youtube.com/watch?v=bNKlXsBnyCA#action=share
  iframe: <iframe width="560" height="315" src="https://www.youtube.com/embed/bNKlXsBnyCA"
    frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
---

Augmented reality is more than just a tool for games and advertising; it now creates real-world utility. Childhood is full of dramatic, beautiful and intense dreams. As a child’s cognitive and behavioral skills grow, creativity and imagination fill in the details and color in the blanks. Philosopher Jean-Jacques Rousseau described childhood as “the sleep of reason,” and childhood imagination is a key part of that sleep.
In children’s imaginations, a hero defeats the largest dragon in the enchanted forest and saves the beautiful princess from her evil stepsisters. During your childhood, maybe you had a tea party with your imaginary friends and your stuffed animals. When you were chatting away about how amazing the not-real food and drinks tasted, reason slept.

No longer do the fantasy worlds of our childhood have to be in the past. With augmented reality, adults can now start to relive their imaginative childhoods and bring play back into our lives. As we’ve seen with social sensation Pokémon Go, AR helps adults play with their surroundings and explore the real world around them.

Augmented reality brings back the childhood joy of imagination and world-building, but in a much more realistic and visceral way with the tap of our phones. The best part? We aren’t limited to 11×13 sheets of white paper to draw on — in this new world of the imagination combined with tablets and smartphones, the entire space around us becomes our medium and backdrop.

## AR in play
AR paints a canvas where the entire world is your palette. Your canvas is the space around you, including walls, ceilings, and even the air above your head. Using AR, anything in your field of vision can be overlaid with magical interactions to explore.

This is important for us as adults. Children, especially toddlers, have a hard time understanding what is real and what is made up. According to Dr. Paul L. Harris, author of *[The Work of the Imagination](https://www.amazon.com/Work-Imagination-Paul-L-Harris/dp/0631218866)*, distinguishing fantasy from reality is an ongoing process that takes place at different ages for different children. This period is crucial to childhood development, while adding splendor and fun to their lives in the process.

As adults, we are taught to move beyond “childlike” behaviors mixing fantasy and reality. One of the side effects of this process is losing easy access to the naivete and surprise from constant novelty that many children have. We also lose the sense of living in the dream-like state of early childhood that allows us to create surreal worlds.

Now, with smartphones and AR, we can bring imagination back into our lives and act like kids again. For instance, making up absurd settings and environments can change serious situations into funny ones. Or you can put an elephant on your coworkers’ head or show dinosaurs eating the food at your lunch table. You can also create beauty more easily with hundreds of life-like butterflies on the bedroom floor or snow in the middle of your summer beach house.

## AR in practice
Since we can now change our spaces using animated 3D objects, surreal environments or settings are now be possible. The sky’s the limit to what people can create in AR and literally our deepest imaginations can now become a reality in AR.

AR serves two functions: letting our imaginations run rampant in our physical surroundings (just think of the success of Pokemon Go), and generating real-world utility by merging the digital and physical worlds. A range of apps, such as Ikea’s, give us the ability to imagine what a piece of furniture looks like in our living room. Companies like [Total Immersion](http://www.t-immersion.com/) let you see how a pair of earrings match your prom dress. There’s even an AR app for farmers which helps them better understand the correct dosage of vaccines to give to farm animals.

Even more use cases are just around the corner. In the coming years, when you need to fix your dishwasher, there will be an AR app that can help pinpoint the problem and bring the right handyman and tools to help fix it quickly. There’s one in the market today called [Streem](https://www.streem.pro/) that helps solve these day to day problems.

As technology evolves, new opportunities arise. We’ve finally built the tools, through augmented reality, to transform how we interact with our environment. Take even the idea of manifesting your dreams in the real world, like becoming [more rich](https://gph.is/2kMD2NO). AR allows us to shift the focus of technology from ourselves to the world around us. By having a larger breadth of landscapes and places to interact with, augmented reality changes a great many things about day-to-day life.

The best part is that toolkits such as Apple’s ARKit and Google’s ARCore make it easier to experiment with AR and find new use cases. With a little bit of training and experimentation, organizations and individuals can find ways for AR to entertain, engage the imagination, and improve lives.

Because AR is such a powerful tool, both for imagination and for daily life, it offers us a wide range of opportunities. On the psychological front, adults can feel like children again, and daily life improves in a number of categories. Best of all, we can build out our [wildest dreams](https://www.youtube.com/watch?v=eVOqiHP-5e4) in this world.

*[Dana Loberg](https://twitter.com/luckyloberg?lang=en) is a visual artist turned co-founder and CEO of LEO, the first marketplace for augmented visual communication, a growing medium that is revolutionizing the way people interact with each other and the world around them.*