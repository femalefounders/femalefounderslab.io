---
title: MovieLaLa’s desktop GIF Maker will have you ruining Twitter feeds in minutes
layout: article
categories:
- 
image: http://localhost:4000/thenextweb-movielalas.jpg
author: Dana Loberg
date: '2016-06-29 02:00:00'
source:
  name: The Next Web
  url: https://thenextweb.com/apps/2016/01/29/movielalas-desktop-gif-maker-will-have-you-ruining-twitter-feeds-in-minutes/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
---

There are a whole bunch of different ways to create and edit GIFs on the Web, but if you’re after a simple desktop GIF maker, MovieLaLa’s is worth checking out.

It’s free to download and use (with limitations), and available for [Windows](https://thenextweb.com/topic/windows/) and Mac.

Without paying any money, what you get is the ability to create GIFs that are two seconds long. If you upgrade to ‘Pro, ,you can apply filters, create a custom length GIF and can remove MovieLaLa’s watermark. [Pro costs $1.99 a month](http://movielala.com/gif-maker/gif-maker-pricing/) for a single user, and from $4.99 per month for businesses or social media teams.

To cut a simple GIF, you just insert the URL of the video you want to use as the source or upload one from local storage. Free users can then just place the timeline marker for the two second clip to begin, and when happy with it, hit ‘create GIF.’

All credit to [The Slow Mo Guys](https://www.youtube.com/channel/UCUK0HBIBWgM2c4vsPhkYY4w) for [the video](https://www.youtube.com/watch?v=NMbM-ERy2Lk) on [YouTube](https://thenextweb.com/topic/youtube/).

MovieLaLa says that the tool supports 200 other sites in addition to YouTube.

The [end result](https://movielala.com/gifs/6ft-man-in-6ft-giant-water-balloon-4k-the-slow-mo-guys-EkZ20KNYg) is a simple looping GIF that you can download or share directly – once you hit the share button, it’s uploaded to MovieLaLa and can be shared via the URL.

If you decide to pay for the upgrade, which to me would be better as a one-off fee rather than an on-going monthly charge, then you can edit the length of the video and add a few filters in just a few presses.

You can also adjust the settings for things like brightness and exposure manually.

Once you’re happy with the length of the cut, you just need to hit that ‘Create GIF’ button again.

If you don’t like the end result, there’s a button to allow you to go back and tweak the settings.

The end result is an altogether less frustrating GIF, which despite the watermark for demo purposes, is still all credit to The Slo Mo Guys.

You can choose to have a line of text or not regardless of whether you pay.

It’s not perfect – I had to restart the process once when attempting to go back and edit a GIF after creating it because it wouldn’t load – and it’s hard to imagine anyone except social media account managers paying an ongoing monthly fee, but if you want a solid way to make GIFs out of your own videos on the desktop or from sites like YouTube, this is it.

[➤ MovieLaLa GIF Maker](http://movielala.com/gif-maker/)