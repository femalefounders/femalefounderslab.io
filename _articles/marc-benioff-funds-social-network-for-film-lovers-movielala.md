---
layout: article
categories:
- 
image: http://localhost:4000/2af2c99c17599dd370949a1503599173_yd1nxb.jpg
author: Dana Loberg
date: '2014-04-30 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2014/04/30/movielala/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: Marc Benioff Funds Social Network For Film Lovers “MovieLaLa”
---

“Movie studios are spending hundreds of millions of dollars to market movies without the analytics and data to know where to target that marketing and to know it’s working,” says Marc Benioff, CEO of Salesforce. He’s the latest investor in [MovieLaLa](https://movielala.com/), a film fan social network that aims to solve this problem by doubling as a data-driven marketing platform for Hollywood. It plans to launch in the coming weeks.

The idea behind MovieLaLa is that true film lovers are eager for early info on the latest flicks. The site plans to offer them exclusive content and opportunities to connect with stars amongst a community of enthusiasts.

In exchange, data on who they are and what they like will be provided to movie studios to improve the targeting of marketing campaigns. For example, if a studio releases a succesful original film that gains a following on MovieLaLa, they’ll have a way to communicate with that same fan base when it’s time to promote the sequel. This makes marketing much more sustainable, and helps studios build the opening weekend buzz that’s crucial to success.

MovieLaLa will have its work cut out for it convincing users they can’t get enough film news from blogs and their general purpose social networks. Without juicy content and experiences only available on MovieLaLa, users might not bother signing up or coming back. Luckily the startup has CEO Dana Loberg, who has spent years working on big marketing campaigns and at a film studio in Los Angeles.

Benioff’s investment brings the startup to [$750,000 in funding](http://www.crunchbase.com/organization/movielala). He serves as a San Francisco supplement to MovieLala’s other invetors that include Machininma co-founder and CEO Allen DeBevoise, past HBO president of digital initiatives and Warner Bros Online co-founder Jim Moloshok, founding Flixster investor Larry Braitman, and more from the entertainment industry.

If MovieLaLa succeeds, it could stengthen ties between Hollywood and Silicon Valley after an era of digital piracy tore them apart.

_\[**Image Credit**: [Marvel](https://marvel.com/comics/characters/1009368/iron_man)\] _