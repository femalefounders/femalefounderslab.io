---
layout: article
categories:
- Women in Entrepreneur
image: http://localhost:4000/this-yale-grad-could-be-the-next-emoji-millionaire_dire1e_exl3yp-1030x478.jpg
author: Dana Loberg
date: '2017-04-15 02:00:00'
source:
  name: Inc.
  url: https://www.inc.com/kate-l-harrison/this-yale-grad-could-be-the-next-emoji-millionaire.html
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: This Yale Grad Could Be the Next Emoji Millionaire
---

MojiLaLa is billing itself as the Netflix for emojis and it is on track to be huge.

Emojis are already a multi-billion dollar business in Asia and they are [blowing up here in the U.S. as well](https://www.inc.com/damon-brown/why-emojis-are-now-big-business.html?cid=search), with the release of [Apple’s iMessage](https://www.inc.com/alex-moazed/airbnb-just-placed-a-big-bet-on-apples-new-imessage-platform.html?cid=search) sticker store. One company leading the pack is [MojiLaLa](https://mojilala.com/), billed as “a Netflix for emjois.” The company was founded by Dana Loberg and Sahin Boydas in 2016. Loberg is a Yale graduate and artist. Boydas is a serial entrepreneur and technical leader. They both recently graduated from the start-up incubator “500 Startups [Silicon Valley](https://www.inc.com/andrew-medal/how-to-break-into-silicon-valley-without-living-there.html?cid=search).” MojiLaLa gives subscribers unlimited access to 20,000+ emojis for a monthly fee of just $1.99.

**Identifying The Emoji Problem**

The inspiration for MojiLaLa came when Loberg realized she was searching for emoji packages, downloading the package, and then only using one emoji. “Plus,” she adds, “there are a lot of missing emojis out there, especially when it comes to pop culture and media.” The idea of a marketplace for Emojis was born. The company’s mission is to create the “most diverse and original packages in the emoji space, focused on artists and their emojis from all over the world.” The site consolidates all the options into one simple app to use with all of your text. “It’s different. We focus on the artists and getting distribution of their sticker packs across major platforms and keyboards globally,” Loberg adds. “Competitors are focused on building individual keyboards and emoji packages for big businesses or celebrities.”

**Circular Promotion**

The platform helps the 2,000 Emoji artists that use it, and the artists help the platform by promoting it organically. This was a big key to Etsy’s success, and is likely to work in the emoji space as well. To help artists along, the company has press kits and designs for MojiLaLa artists to share their emoji creations through their social channels. Loberg adds: “MojiLaLa is inherently social in the fact that emojis are sent and received daily with over 6 Billion emojis sent every day!”

**Challenges**

When asked about the biggest challenge they face in bringing their product to market, Loberg says, “MojiLaLa has become a third party platform and resource for major chat platforms and keyboards looking for more recent, new and engaging emoji content. While MojiLaLa is focused on building the best community for artists, it’s been a challenge to manage the demand from platforms and businesses who want new packages and licensing deals going forward.”

The company is currently angel funded and is raising a seed round. It has just started being featured in the Apple sticker app store and is currently in the top 10 for the second week in a row. If it continues on its current path, it could do $1M in revenue in less than 3 years. Now that’s something to :) about!