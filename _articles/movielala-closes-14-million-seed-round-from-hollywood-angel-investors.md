---
layout: article
categories:
- 
image: http://localhost:4000/variety-movielala-1-4-million-seed_lfgois_hcp1mx-1030x580.jpg
author: Dana Loberg
date: '2016-01-12 02:00:00'
source:
  name: Variety
  url: http://variety.com/2016/digital/news/movielala-1-4-million-seed-round-hollywood-angel-investors-1201678139/
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: MovieLaLa Closes $1.4 Million Seed Round From Hollywood Angel Investors
---

Social movie-marketing app startup MovieLaLa announced that it has wrapped a $1.4 million seed round of funding from a notable roster of entertainment-industry execs.

The company’s investors include Jonathan Miller, former CEO at News Corp and AOL and recent board member of IPG; Jay Rasulo, former CFO of Walt Disney; David Anderman, former COO of LucasFilm; Jim Wyatt, former CEO of WME; and Gregory Milken, managing director at March Capital Partners and son of financier Michael Milken.

Other MovieLaLa backers include Salesforce CEO Marc Benioff, Machinima chairman Allen DeBevoise, Jim Moloshok, former president of Warner Bros. Digital, Adam Nash, former VP of product at LinkedIn, Peter Csathy, CEO of Manatt Digital Media, and movie producer Angie Wang.

San Francisco-based MovieLaLa, founded in 2012, will use the financing to expand mobile distribution and engagement of Hollywood studio content, as well as provide studios with data about consumer behavior, according to Dana Loberg, CEO and co-founder of MovieLaLa. The 10-person startup is now looking to raise Series A financing.

“We’re incredibly fortunate to have the backing of such an impressive team of investors, all of whom are deeply rooted in entertainment and see what is becoming crucial to movie marketing as users become more dependent on mobile,” said Loberg.

MovieLaLa’s app for Apple iOS devices provides a social network for users to share and discover upcoming movies through a personalized feed, find friends to go to movies with, purchase tickets and receive updates about movies they love.

Rasulo, who [stepped down as Disney’s CFO last June](http://variety.com/2015/film/news/disney-cfo-jay-rasulo-stepping-down-at-months-end-1201509331/), said in a prepared statement, “In the digital era, studios are looking for more solutions, using new technologies to provide essential tools to learn more about their audience, get more reach and broader distribution through various mobile apps on multiple platforms. The MovieLaLa platform is working on this vision that can create a real impact on movie marketing in Hollywood.”