---
layout: article
categories:
- Tech
image: http://localhost:4000/500-startups_ovfhqf_qho4dy-1030x579.jpg
author: Dana Loberg
date: '2016-11-18 02:00:00'
source:
  name: Tech Crunch
  url: https://techcrunch.com/2016/11/18/heres-the-19th-batch-of-500-startups-companies/
tags:
- Articles
- Tech
video:
  title: ''
  url: ''
  iframe: ''
title: Here’s the 19th batch of 500 Startups companies
---

500 Startups  just wrapped up its 18th batch of company launches last month — which means it’s already looking forward to the next one.

This time around, in its 19th batch, the program ([no longer called an accelerator](https://beta.techcrunch.com/wp-admin/post.php?post=1418260&action=edit), ironically) has 44 companies, ranging from business-to-business software to beauty products. You’ll find everything from software for managing fitness businesses to beauty subscription services geared toward more targeted markets. Nearly half the companies come from outside the U.S..

Here’s a quick rundown of all the companies passing through the program this time around:
*   [Aella Credit](http://www.aellacredit.com/) — Online lending platform for unbanked and underbanked individuals in emerging markets.
*   [Almabase](http://almabase.com/) — Software for schools to increase alumni involvement and donations.
*   [Ambience Data Inc.](http://ambiencedata.com/) — IoT software with zero footprint client infrastructure focusing on environmental monitoring.
*   [aumet](https://aumet.me/) — Software for matching medical suppliers with distributors.
*   [Baloonr](http://baloonr.com/) — Removing bias from group work and decision-making.
*   [Bstow, Inc.](https://www.bstow.com/) — Round up spare change for charity.
*   Cardlife LTD — Business subscriptions management.
*   [ChangeJar Inc.](http://www.changejar.com/) — A mobile cash platform optimized for small retail payments.
*   [ClaimCompass](http://www.claimcompass.eu/en) — Get your compensation for a delayed or cancelled flight.
*   [CloudCoffer](http://www.cloudcoffer.com/) — Smart cyber threat defense and privacy protection system.
*   [Crema Co](https://crema.co/) — A coffee marketplace that enables you to subscribe to roasted-to-order coffees from top roasters.
*   [EventXtra](http://eventxtra.com/) — Enterprise one-stop event management platform.
*   [Exipple Studio](http://www.gestoos.com/) — A computer vision AI software platform that enables camera (sensor) equipped devices to see and understand natural gestures and behavior.
*   [Fingertips Lab](http://www.o6app.com/) — Use mobile apps without either looking at or touching a screen.
*   [Fuel, Inc](http://www.tryfuel.com/) — Use your fitness, biomarker and DNA data to optimize your nutrition and your body.
*   [GAS POS INC. (Fuel for Clover)](http://gaspos.co/) — Sales tools for petroleum and gas.
*   Gluwa Inc. — Twilio for banking.
*   [GymHit Software](http://www.gymhit.com/) — Business management and marketing software for the fitness industry.
*   [Homigo Home Services Corp.](http://www.homigo.com/) — Concierge service for managing home maintenance.
*   [Hykso Inc.](http://www.hykso.com/) — Motion sensors for boxing and MMA.
*   [iControl](http://www.icontrolapp.se/en/) — A cloud-based app for construction collaboration and documentation.
*   [Idwall Tecnologia LTDA – ME](http://idwall.co/) — Identify verification, compliance assurance and anti-fraud in a single a platform.
*   [InnerSpace Technology Inc.](http://www.innerspace.io/) — A turnkey platform that provides instant indoor maps and accurate real-time positioning of people and things.
*   KiLife Tech, Inc. — Wearables for keeping family members connected.
*   [Kompyte](http://www.kompyte.com/) — Software to track competitors in real time.
*   LawTova Incorporated — Marketplace facilitating a meeting between attorneys and legal services support staff for jobs and projects.
*   [Mashvisor](https://www.mashvisor.com/) — An online platform that provides real estate analytics to help investors find traditional and Airbnb investment properties.
*   [MojiLaLa Inc](http://mojilala.com/) — Emoji Marketplace for independent designers and studios from around the world.
*   [MyFavorito GmbH](http://www.myfavorito.com/) — B2C CRM and customer engagement platform for brands, retailers and store owners that generates measurable customer relationships, store visits, referrals, loyalty and sales, not likes.
*   [Open Door Group PBC](http://www.opendoor.io/) — OpenDoor creates co-living spaces for more meaningful lives.
*   [OWLR](http://www.owlr.com/) — Home security camera monitoring.
*   [Park Evergreen](http://www.parkevergreen.com/) — Software for managing parking for offices.
*   [Pawprint, Inc.](http://www.getpawprint.com/) — Tools for booking appointments with veterinarians and managing your pet’s health.
*   [Pluto AI, Inc.](http://www.plutoai.com/) — AI powered analytics platform for water management.
*   [RocketBolt](http://rocketbolt.com/) — Contact monitoring tool for tracking sales prospects on websites, in the news and across social media.
*   [Scopio](http://www.scop.io/) — A search engine to find and license images on social media.
*   [ShearShare, Inc.](http://shearshare.com/) — Airbnb for beauty professionals.
*   [Sickweather](http://www.sickweather.com/) — The doppler radar for avoiding getting sick.
*   [Tagove](https://www.tagove.com/) — A live chat software with video chat and co-browsing, allowing businesses to have face-to-face online conversations with their customers within the website.
*   [TalentBase](http://www.talentbase.ng/) — HR software for growing enterprises in Africa.
*   [Teleport](http://www.teleport.ninja/) — Interactive, scrollable, media-enriched online video.
*   [Teleport Enterprises, Inc.](http://flye.co/) — Location-based, social intelligence and engagement platform.
*   [UNICORN](http://unicornskincare.com/) — A skin care brand geared toward gay men.
*   [We Are Onyx](http://www.weareonyx.com/) — A beauty subscription service geared toward black women.