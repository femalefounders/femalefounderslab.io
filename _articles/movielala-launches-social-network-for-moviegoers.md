---
layout: article
categories:
- 
image: http://localhost:4000/hollywoodreporter-movielala-launches-social-network-moviegoers_ltwmoe-1_tua2ac.jpg
author: Dana Loberg
date: '2015-01-01'
source:
  name: Hollywood Reporter
  url: https://www.hollywoodreporter.com/behind-screen/movielala-launches-social-network-moviegoers-703618
tags:
- Articles
video:
  title: ''
  url: ''
  iframe: ''
title: MovieLaLa Launches Social Network for Moviegoers
---

**Its iPad app debuts on Tuesday, with an iPhone and Android app set to follow in the coming weeks.**
MovieLaLa, a San Francisco-based startup with seed funding provided by individuals including [SalesForce.com](https://www.salesforce.com) CEO **Marc Benioff** and Machinima co-founder **Allen DeBevoise**, is launching a social network for movie fans and a potential movie marketing platform for studios.

Its iPad app debuts on Tuesday, and iPhone and Android versions are expected to be available in the coming weeks.

**[PHOTOS: When Movie Palaces Reigned](https://www.hollywoodreporter.com/gallery/movie-palaces-reigned-photos-697806)**

“The social experience is a huge part of moviegoing, but it previously hadn’t successfully made the jump to our online lives,” said CEO and co-founder **Dana Loberg**.

The app’s features include coming attractions and trailers through a personalized Hollywood feed and the ability to share with friends through the app’s social network. It also offers an “icebreaker” feature that allows users to meet additional fans with whom to go to the movies. (Loberg acknowledged that the later sounds like a potential online dating component, though she added, “We didn’t intend [the feature to be used] that way.”)

To develop the movie marketing aspect of the app, the startup is talking with execs at roughly a dozen studios including Disney, Fox and Sony to gain feedback. “Hollywood is based on person-to-person relationships,” Loberg said. “We’re hoping to improve the system for them.”

Investors include **Jim Moloshok**, a former HBO and Warner Bros. exec, and **Larry Braitman**, a founding investor in Flixster.

Email: Carolyn.Giardina@THR.com
Twitter: @CGinLA