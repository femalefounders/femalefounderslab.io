---
title: Celebrating women in tech&#58; Meet Dana Loberg, CEO and Founder of LEO AR
layout: article
categories:
- Women in Entrepreneur
image: http://localhost:4000/imore-leo-ar-mockup.jpeg
author: Lory Gil
date: '2019-04-18 02:00:00'
source:
  name: Where Women Create
  url: https://www.imore.com/celebrating-women-tech-meet-dana-loberg-ceo-and-founder-leo-ar
tags:
- Women in Entrepreneur
- Tech
- ''
video:
  title: ''
  url: ''
  iframe: ''
---

Augmented reality (AR) hasn't quite hit the mainstream, but it's getting there in a big way. [Even Apple is excited](https://www.imore.com/arkit) about the future of AR. It's universally useful and fun. You can see what a couch looks like in your living room or make a mini-movie staring yourself and R2-D2.

Leo AR is an entertainment app that takes the idea of stickers to the next level. When you open Leo AR, it triggers a camera with a tray of various "stickers" that you can add right onto the screen. These are more than just stickers, though. They move, change size, and can be viewed from any angle in a 3-D way. Drop a skeleton into your living room and record your kids running from it (or dancing with it) from any angle. I just dropped a bunch of bunnies in my backyard and made it look like I had an infestation.

**[Get Leo AR in the App Store](https://itunes.apple.com/us/app/leo-augmented-reality-camera/id1286981298?mt=8&at=10l3Vy&ct=UUimUdUnU52462YYwYd)**

Leo AR's founder and CEO, Dana Logberg started her higher education as an artist and history major, followed up by starting her own business, [MojiLaLa](https://itunes.apple.com/us/app/stickers-for-photos-for-text/id1089497832?at=10l3Vy&ct=UUimUdUnU52462YYwYd), which is still hugely popular and works with iMessage (so much sticker fun), and then joined StartX, a founders collective, as a serial entrepreneur. She's lived on both coasts of the U.S. and has learned a lot during her career.

I was able to chat with Dana through email recently about her beginnings, the current state of women in tech, and her secretive plans for the future.

As an artist with a history major, what brought you into the world of tech?

Ironically, I wrote my history thesis at Yale on the influence of technology on religion. I was always super fascinated with religion and the impact religion has had on the evolutions of societies and humanity throughout time. Unknowingly, my fascination with belief systems and also studying how technology can completely revolutionize societies' lifestyles and ways of thinking (think of the wheel, fire, steam engine, etc.) was always even more intriguing to me. New technology can really shift peoples' thoughts and expand into new ideas that really push the boundaries of civilizations into new eras.

Today, the technology of our time has such major impacts on our lives but in a much faster and more dynamic pace than ever before. Bringing this back to religion, some would even say Apple stores have a church-like experience for consumers with their high glass ceilings and passionate fans for some of the hardware devices. I never thought that I would live in the mecca of technology, here in Silicon Valley, back then in college. Nor did I have any idea that I would be working in tech running my own startup.

> I packed up my bags and sold everything I owned and moved to San Francisco without ever seeing or actually visiting before.

After college, I moved to the West Village in NYC to be close to the Chelsea art markets. It was also an opportunity to work in one of the largest ad agencies of the world --- McCann Erickson --- where I could flex my writing and research skills as a copywriter while still innovating on new ways to market brands visually (using my skills as both a writer and artist). On a whim, after 6 years in NYC, I packed up my bags and sold everything I owned and moved to San Francisco without ever seeing or actually visiting before. I guess I was a risk taker at heart from the get-go. I was looking for a completely different experience than what NYC had to offer: away from the corporate ladder, glass ceilings and something smaller and foreign to traditional east coast jobs. My first job in SF was at Bhava Communications, a boutique PR agency for high-end tech companies. I was one of the first employees and it was run by a woman. The rest is history.

You've had a lot of success with projects you've started in the past; MojiLaLa and MovieLaLa, and Leo AR. Talk about your experience starting out with MojiLaLa compared to your experience now with Leo AR.

We see the changes happening from chat platforms to the camera being the next major platform. Chat was 10 years ago. Camera is the next 10 years.

When we started MojiLaLa, building one of the largest 2D sticker marketplaces, we believed in stickers augmenting text in chat (in some cases replacing text). Then we started to see stickers move from chat to camera where people could drag and drop stickers on to pictures, which is what we see today. The next wave will move from 2D to 3D with the next digital layer happening not in text, not on pictures --- but in video with consumers adding realistic 3D objects to the real world. It's a behavior that already exists today with layering your photos with filters, text and stickers; next we'll see people layering the entire world with AR objects, AR text and AR filters. MojiLaLa and Leo AR is an evolution of 2D moving into the 3D world, with the massive opportunity to own the consumer-facing AR market with Leo AR.

As a member of StartX, are you able to meet and work with other women doing something similar?

StartX is filled with genius founders and I'm always grateful to be surrounded by such incredibly smart talent. Some people are diagnosing cancer using the camera on our phones, while others are doing 3D mapping of construction sites. I have yet to find other women in StartX doing something similar, but the batch is small and very diverse, and that doesn't mean there won't be more coming up doing something in AR very soon.

What are your next steps? Do you have any ideas for future projects?

Next steps are focused on product and building out the Leo AR community. I always have ideas brewing ;)

Are you considering exploring any art project?

I wish I carved out more time for art these days. But I have an unfinished art piece in my workroom that I plan to tackle on the weekends. Art will always be a part of my life and I'll get back to doing it more when the time allows.

For young women learning about the tech world and making decisions about getting into either programming or starting a company, based on your experience, what's one thing you'd wish you knew getting started?

> Failure is actually putting you more in the right path to reach greater success.

I wish I knew that it's ok to fail. And it's actually a great way to end something and start something new. My perception of failure, in the past, was always so negative. And now looking back, failure is actually putting you more in the right path to reach greater success. So embrace your failures and weaknesses and learn from them as much as you can. And know that it's taking you one step forward in the right direction.

As an entrepreneur, you've had a lot of chances to interact with other tech company creators. Have you met a lot of other women entrepreneurs in tech?

I have been lucky to meet and be friends with other women in tech, and even not in tech. There's definitely a great initiative in the valley to diversify the founders and I do see a lot of other women building tech companies. As a whole, there's still a long way to go until the ratios are balanced out between male founders and female founders. I do love seeing more women jump into tech.