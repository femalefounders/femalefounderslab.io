---
title: Faces
layout: artwork
image: http://localhost:4000/691e2425242490.5604bc9b20f37_rfjt4o_ba8lnv.jpg
gallery: faces
---

The face, in particular the eyes, has always been central to my work. In this collection I set out to explore how color and tone change the viewer’s perception of her face and mood: beautiful, soft, cold, warm, surreal, real, anime. The figures are painted on torn pages of poetry by E. E. Cummings, or Janet Fitch’s The White Oleander: adding a layer of verbal imagery as well as physical texture.
I use torn pages from books, newspapers, and hand-written poetry to remind the viewer that our person and personality are composed of imperfect, fragile and fluid fragments, both physical and mental. Both body and mind develop consciously and unconsciously over the course of our lives. The printed words reflect how we acquire knowledge through education and linear experience. Meanwhile, the haphazard layering of multiple pieces of information form formal and informal sources represents how we store, recover and reinterpret that knowledge through conscious and unconscious processes. Those processes are developing and adapting continuously as fragments are added, moved, linked, unlinked and lost. Memory and perception, therefore, are both unique to each of us but are composed of fragments of others.