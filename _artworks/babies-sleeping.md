---
layout: artwork
title: Babies Sleeping
image: http://localhost:4000/4f8bf55978493.5602584be3d8e_nz7bih_iutaim.jpg
gallery: babies_sleeping
---

Tan, red and blue charcoal were used to paint the delicate image of a resting baby. The image is overlaid onto pages torn from the popular children’s books by Shel Silverstein: “The Missing Piece” and “The Giving Tree”. Silversteins books have a simple poetic structure, which is mimicked by the simple black and white hand-drawn images. The innocent simplicity of these images and words of childhood floats over the young child, accentuating the peacefulness of its sleep. However, these chopped images break up the otherwise smooth surface, bringing a sense of fragility and imperfection of language, memory, and life.

The final (blue) image overlays poetry written by Erica Loberg, the artist’s elder sister, onto the image of the sleeping baby. This addition brings complexity of thought and perception to an otherwise seemingly placid child.