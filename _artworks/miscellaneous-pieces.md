---
layout: artwork
title: Miscellaneous Pieces
image: http://localhost:4000/8ded579231119.5602d64da0ae4-1_xams10_ldykbt.jpg
gallery: miscellaneous_pieces
---

Painted with charcoal: in the background are childhood memories from miscellaneous gallery viewers. There are also torn pages from J.D. Salinger’s novel The Catcher in the Rye.