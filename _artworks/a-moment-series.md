---
layout: artwork
title: A Moment Series
image: http://localhost:4000/bab4295977252.560257984b46c_cocu2t_riabrm.jpg
gallery: a_moment_series
---

The image of a child looking over its shoulder is a favorite of Dana’s. In this series, charcoal is painted onto torn pages from the popular children’s books by Shel Silverstein: “The Giving Tree”, “The Missing Pieces Meets the Big O”, and “Where the Sidewalk Ends”. These timeless books, written in Silverstein’s unique poetic style, introduce children to concepts that help bring meaning to life. The collage of torn pages and incomplete story lines makes the image of the child more fragile, delicate, and imperfect while surrounded by these thoughts.

Selected paintings include handwritten poetry by Dana’s sister, Erica Loberg. Erica’s unique and modern prose is a raw edge against the simple words and Silverstein’s drawings. This combination of memories, stories, thoughts and emotions accentuate the conflict and confusion which occurs where the internal and external worlds collide and where child-like innocence and adult complexity attempt to coexist.