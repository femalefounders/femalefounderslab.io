---
layout: artwork
title: Cherub Series
image: http://localhost:4000/90d4375978144.560257ab907d9_fifdkm_ezrf6m.jpg
gallery: cherub_series
---

The five cherubs total in this collection represent a contemporary interpretation of the classic image of the Renaissance cherub. Each cherub has a unique personality represented by both pose and coloring. Meanwhile, the cherub’s engagement with the lute contrasts with traditional static and disinterested poses, imbuing the cherub with a sense of humanity and character. The mixed media approach incorporates charcoal on paper and a collage of torn pages from Christian scripture and religious texts as well as, classical sheet music. The wings are made of appliqué feathers, which add perspective and texture but also accentuate each angel’s individual and dynamic nature.